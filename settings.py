'''

This file contains configuration parameters (paths + parameters) that Cr.In. might need.

'''

##
# PATH settings go here.
##

FIREBUG_PATH = "./extensions/firebug-2.0.8.xpi"
ENABLE_FIREBUG = True
NETEXPORT_PATH = "./extensions/netExport-0.9b7.xpi"
ENABLE_NETEXPORT = True
NOSCRIPT_PATH = "./extensions/noscript-2.6.9.3.xpi"
ENABLE_NOSCRIPT = False
ADBLOCK_PATH = "./extensions/adblock_plus-2.6.12.xpi"
ENABLE_ADBLOCK = False
GHOSTERY_PATH = "./extensions/ghostery-5.4.8.xpi"
GHOSTERY_PREFS = "./extensions/ghostery_store.json"
ENABLE_GHOSTERY = False
PRIVACY_BADGER_PATH = "./extensions/privacy_badger-1.0.1.xpi"
PRIVACY_BADGER_PREFS = "./extensions/privacy_badger_store.json"
ENABLE_PRIVACY_BADGER = False

FIREFOX_BINARY_PATH = "./firefox/firefox"

TOR_DATA_DIRECTORY_PATH = "./tor-dd/"
TOR_PATH = "/usr/sbin/tor"

CREDENTIALS_PATH = "./login-test-credentials"

##
# PARAM settings go here.
##

TOR_INIT_TIME = 30
TOR_KILL_RETRIES = 10

DRIVER_MAX_PAGE_LOAD_TIME = 60
DRIVER_QUIT_RETRIES = 10
DRIVER_DELETE_COOKIES = True
DRIVER_ALERT_WAIT_TIME = 1
DRIVER_SCREENSHOT_RETRIES = 3
DRIVER_HTML_SAVE_RETRIES = 3
DRIVER_LOGIN_MAX_INTERACTIONS = 10
DRIVER_ACCOUNT_CREATION_MAX_INTERACTIONS = 10
DRIVER_COOKIE_SAVE_RETRIES = 3
DRIVER_POST_MAX_INTERACTIONS = 10

TSHARK_RECORDER = False
TSHARK_INTERFACE = "em1"

DISPLAY_CLOSE_RETRIES = 10

EXPERIMENT_PAGE_LOAD_RETRIES = 3

FUNCTION_TIMEOUT = 120
