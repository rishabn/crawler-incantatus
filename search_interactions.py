'''

Functions dealing with search interactions go in this class.
Add new templates for search interactions here.

'''

import logging
import utils
import dom_interactions

class SearchInteractions:

    def __init__(self, driver):
        self.driver = driver
        self.di = dom_interactions.DomInteractions(driver)
        self.input_elements = self.di.get_all_input_elements()
        self.search_rule, self.search_element = None, None
        self.interactions = list()

    def __check_single_input_rule(self):
        # If there is only a single input box, it is usually a search box
        rule, element = False, None
        if len(self.input_elements) == 1:
            rule = True
            element = self.input_elements[0]
        logging.debug("Search rule: Single input box [ %s | %s ]" % (str(rule), utils.dict_to_json(element)))
        return element, rule

    def __check_single_maxlen_input_rule(self):
        # If there is only a single input box with a defined maxlen, it is usually a search box
        candidates = list()
        rule, element = False, None
        for ie in self.input_elements:
            if (utils.is_not_none(ie['max-length'])):
                candidates.append(ie)
        if len(candidates) == 1:
            rule = True
            element = candidates[0]
        logging.debug("Search rule: Single input box with defined maxlen [ %s | %s ]" % (str(rule), utils.dict_to_json(element)))
        return element, rule

    def __check_single_text_box(self):
        # If there is only a single text box, it is usually a search box
        candidates = list()
        rule, element = False, None
        for ie in self.input_elements:
            if ie['type'] == unicode("text"):
                candidates.append(ie)
        if len(candidates) == 1:
            rule = True
            element = candidates[0]
        logging.debug("Search rule: Single text box [ %s | %s ]" % (str(rule), utils.dict_to_json(element)))
        return element, rule

    def __check_text_keyword_rule(self):
        # If there is a textbox with one of its attributes containing a search related keyword, it is usually a search box
        candidates = list()
        keywords = [unicode("q"), unicode("query"), unicode("search"), unicode("querytext")]
        rule, element = False, None
        for ie in self.input_elements:
            strings = [ie['name'], ie['label'], ie['title']]
            if (utils.at_least_one_in_keywords(strings, keywords)) and (ie['type'] == unicode("text")):
                candidates.append(ie)
        if len(candidates) > 0:
            rule = True
            element = candidates[-1]
        logging.debug("Search rule: Text box and keywords rule [ %s | %s ]" % (str(rule), utils.dict_to_json(element)))
        return element, rule

    def __check_keyword_rule(self):
        # If there is any input with one of its attributes containing a search related keyword, it is usually a search box
        candidates = list()
        keywords = [unicode("q"), unicode("query"), unicode("search"), unicode("querytext")]
        rule, element = False, None
        for ie in self.input_elements:
            strings = [ie['name'], ie['label'], ie['title']]
            if (utils.at_least_one_in_keywords(strings, keywords)):
                candidates.append(ie)
        if len(candidates) > 0:
            rule = True
            element = candidates[-1]
        logging.debug("Search rule: Input box and keywords rule [ %s | %s ]" % (str(rule), utils.dict_to_json(element)))
        return element, rule

    def __get_search_element_and_rule(self):
        element, rule = self.__check_single_input_rule()
        if (rule is True) and (len(element['id']) > 0):
            logging.debug("Search Element : %s | Rule: %s" % (element, "Single Input Rule"))
            self.search_rule, self.search_element = "SingleInput", element
            return None
        element, rule = self.__check_single_maxlen_input_rule()
        if (rule is True) and (len(element['id']) > 0):
            logging.debug("Search Element : %s | Rule: %s" % (element, "Single Input with Maxlen Rule"))
            self.search_rule, self.search_element =  "SingleInputMaxlen", element
            return None
        element, rule = self.__check_single_text_box()
        if (rule is True) and (len(element['id']) > 0):
            logging.debug("Search Element : %s | Rule: %s" % (element, "Single Textbox Rule"))
            self.search_rule, self.search_element = "SingleTextbox", element
            return None
        element, rule = self.__check_text_keyword_rule()
        if (rule is True) and (len(element['id']) > 0):
            logging.debug("Search Element : %s | Rule: %s" % (element, "Textbox with Keyword Rule"))
            self.search_rule, self.search_element = "TextboxKeyword", element
            return None
        element, rule = self.__check_keyword_rule()
        if (rule is True) and (len(element['id']) > 0):
            logging.debug("Search Element : %s | Rule: %s" % (element, "Input with Keyword Rule"))
            self.search_rule, self.search_element = "InputKeyword", element
            return None
        self.search_rule, self.search_element = "NoSearchFound", None
        return None

    def get_search_interactions(self):
        self.__get_search_element_and_rule()
        if (self.search_element != "None") and (self.search_element is not None):
            self.interactions.append({'interaction':"click", 'element': self.search_element})
            self.interactions.append({'interaction':"type|stonybrook", 'element':self.search_element})
            self.interactions.append({'interaction':"return", 'element':self.search_element})

