'''

Handles parsing command line parameters + initializes logging.

'''


import argparse
import sys
import logging
import utils
import json


class ArgumentParser:

    def __init__(self):
        args = self.__parse_args()
        root = logging.getLogger()
        if int(args.start_index) >= int(args.end_index):
            self.sites = [] #utils.read_list_from_file(args.url_list)[int(args.end_index)-1:int(args.end_index)]
        else:
            self.sites = utils.read_list_from_file(args.url_list)[int(args.start_index):int(args.end_index)]
        self.display = args.virtualdisplay
        self.tag = args.tag
        self.start = int(args.start_index)
        if args.port:
            if args.exit:
                self.tor_exit, self.tor_port = args.exit, args.port
            else:
                self.tor_exit, self.tor_port = None, args.port
        else:
            self.tor_exit, self.tor_port = None, None
        self.capturepath = args.capture_path + args.tag
        utils.create_folder(self.capturepath)
        self.action = args.action
        if args.replay:
            self.replay = args.replay
            self.replay_log = self.__parse_replay_log()
        else:
            self.replay, self.replay_log = None, None
        if args.vpn:
            self.vpn = args.vpn
            self.vpn_cc = args.vpnloc
        else:
            self.vpn, self.vpn_cc = None, None
        if root.handlers:
            for handler in root.handlers:
                root.removeHandler(handler)
        if args.debug:
            logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)-8s %(filename)s:%(lineno)-4d: %(message)s', datefmt='%m-%d %H:%M', filename= self.capturepath + "/debug.log")
            logging.info("Debug level logging info enabled")
            logging.StreamHandler(sys.stdout)
        else:
            logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-8s %(filename)s:%(lineno)-4d: %(message)s', datefmt='%m-%d %H:%M', filename= self.capturepath + "/debug.log")
            logging.StreamHandler(sys.stdout)
        self.print_parameters()

    @staticmethod
    def __parse_args(print_help=False):
        parser = argparse.ArgumentParser()
        parser.add_argument('--debug', '-d', help='Enable debug logging', action='store_true')
        parser.add_argument('--virtualdisplay', '-vd', help='Use virtual display (headless mode)', action='store_true')
        parser.add_argument('--sitelist', '-sl', help='Path to list of sites to crawl', dest='url_list', required=True)
        parser.add_argument('--start', '-s', help='Start index in sitelist',dest='start_index', required=True)
        parser.add_argument('--end', '-e', help='End index in sitelist', dest='end_index', required=True)
        parser.add_argument('--processtag', '-t', help='Tag to be used for experiment generated data', dest='tag', required=True)
        parser.add_argument('--torexit', '-exit', help='Perform a crawl using specified Tor exit relay', dest='exit')
        parser.add_argument('--torport', '-port', help='Port running the Tor proxy', dest='port')
        parser.add_argument('--capturepath', '-cp', help='Capture path', dest='capture_path', required=True)
        parser.add_argument('--action', '-a', help='Action (Front/Search/Login/Create/Replay)', dest='action', required=True)
        parser.add_argument('--replay-log', '-rl', help='Log containing replay interactions (must be specified for replay actions)', dest='replay')
        parser.add_argument('--vpn', '-vpn', help='Enable a VPN before starting the crawl', action='store_true')
        parser.add_argument('--vpnlocation', '-vpnloc', help='Set the location of the VPN server (needs 2 letter country code)', dest='vpnloc')
        parsed_args = parser.parse_args()
        if parsed_args.action == "replay" and not parsed_args.replay:
            parser.print_help()
            sys.exit(0)
        if print_help:
            parser.print_help()
            sys.exit(0)
        return parsed_args

    def __parse_replay_log(self):
        f = open(self.replay)
        interactions = list()
        for line in f:
            entry = json.loads(line)
            interactions.append(entry)
        return interactions

    def print_parameters(self):
        s = "Sites: [ %d | %s ]" %(len(self.sites), ', '.join(self.sites))
        s += "\t TorParams: [ %s | %s ]" %(self.tor_exit, self.tor_port)
        s += "\t ExperimentParams: [ Display: %s | Tag: %s | CapturePath: %s | Action: %s ] " %(self.display, self.tag, self.capturepath, self.action)
        logging.info("%s" %s)

    def get_params(self):
        params = dict()
        params["sites"] = self.sites
        params["tor-exit"], params["tor-port"] = self.tor_exit, self.tor_port
        params["vpn"] = self.vpn
        params["vpn-loc"] = self.vpn_cc
        if params["tor-port"] is None:
            params["tor-exit"], params["tor-port"] = None, None
            params["tor"] = False
        else:
            params["tor"] = True
            if params["tor-exit"] is None:
                params["tor-exit"] = None
        params["display"], params["tag"], params["capture-path"] = self.display, self.tag, self.capturepath
        params["action"] = self.action
        try:
            params["replay-log"] = self.replay_log
        except AttributeError:
            pass
        return params

    def set_params(self, values):
        try:
            self.sites = values["sites"]
            self.tor_exit, self.tor_port = values["tor-exit"], values["tor-port"]
            self.display, self.tag, self.capturepath = values["display"], values["tag"], values["capture-path"]
            self.action = values["action"]
        except KeyError as ex:
            logging.info("Exception (%s) occurred while setting parameters from dictionary " % ex)
