import random
import json
import logging
import sys

reload(sys)
sys.setdefaultencoding('utf8')


def find_matching_vpn(country_code, select_only_hma=False, select_only_ipvanish=False):
    import pycountry
    country_map = dict()
    for country in list(pycountry.countries):
        country_map[str(country.alpha2).lower().strip()] = str(country.name).lower().strip()
        country_map[str(country.name).lower().strip()] = str(country.alpha2).lower().strip()
    json_ip_map_file = open("./walking_vpn_pure/ip2node.json").read()
    ip_map = json.loads(json_ip_map_file)
    file_map = {v: k for k, v in ip_map.items()}
    json_vpn_dict_file = open("./walking_vpn_pure/vpn_dict.json").read()
    input_data = json.loads(json_vpn_dict_file)
    hma_vpns, ipvanish_vpns = input_data['hma'], input_data['ipvanish']
    hma_keys, ipvanish_keys = list(), list()
    valid_vpns_hma, valid_vpns_ipvanish = list(), list()
    for vpn_ in hma_vpns.keys():
        hma_keys.append(vpn_)
    for vpn_ in ipvanish_vpns.keys():
        ipvanish_keys.append(vpn_)
    for key_ in hma_keys:
        vpn_ = hma_vpns[key_]
        if vpn_['valid'] is False:
            continue
        try:
            if (country_map[vpn_['country_by_ip'].lower().strip()] == country_code) or (
                        vpn_['country_by_ip'].lower().strip() == country_code):
                valid_vpns_hma.append(key_)
            else:
                continue
        except Exception as e:
            continue
    for key_ in ipvanish_keys:
        vpn_ = ipvanish_vpns[key_]
        if vpn_['valid'] is False:
            continue
        try:
            if (country_map[vpn_['country_by_ip'].lower().strip()] == country_code) or (
                        vpn_['country_by_ip'].lower().strip() == country_code):
                valid_vpns_ipvanish.append(key_)
            else:
                continue
        except Exception as e:
            continue
    all_valid_vpns = valid_vpns_hma + valid_vpns_ipvanish
    for vpn_ in all_valid_vpns:
        try:
            file_map[vpn_]
        except KeyError as e:
            all_valid_vpns.remove(vpn_)
            continue
    for vpn_ in valid_vpns_hma:
        try:
            if vpn_ not in all_valid_vpns:
                valid_vpns_hma.remove(vpn_)
        except Exception:
            continue
    for vpn_ in valid_vpns_ipvanish:
        try:
            if vpn_ not in all_valid_vpns:
               valid_vpns_ipvanish.remove(vpn_)
        except Exception:
            continue
    logging.info("There are %d IP-Vanish VPNs and %d HMA VPNs for Country: %s" % (len(valid_vpns_ipvanish), len(valid_vpns_hma), country_code))
    vpn_config, auth_file, cert, ip_address = None, None, None, None
    if (select_only_hma is False) and (select_only_ipvanish is False):
        if len(all_valid_vpns) == 0:
            return None
        # vpn_provider = random.randint(0, 3)
        if (len(valid_vpns_hma) == 0) or (country_code.lower() == "ua"):
            select_only_ipvanish = True
        else:
            select_only_hma = True
    if select_only_hma is True:
        if len(valid_vpns_hma) == 1:
            index = 0
        else:
            index = random.randint(0, len(valid_vpns_hma) - 1)
        vpn_config = "./walking_vpn_pure/walking_hma/vpns/" + file_map[valid_vpns_hma[index]] # Return the first HMA VPN
        auth_file = "./walking_vpn_pure/walking_hma/auth_file"
        cert = None
        for key in ip_map:
            if ip_map[key] == valid_vpns_hma[index]:
                ip_address = key.split(".ovpn")[0]
    elif select_only_ipvanish is True:
        if len(valid_vpns_ipvanish) == 1:
            index = 0
        else:
            index = random.randint(0, len(valid_vpns_ipvanish) - 1)
        vpn_config = "./walking_vpn_pure/walking_ipvanish/vpns/" + file_map[valid_vpns_ipvanish[index]] # Return the first IPVanish VPN
        auth_file = "./walking_vpn_pure/walking_ipvanish/auth_file"
        cert = "./walking_vpn_pure/walking_ipvanish/ca.ipvanish.com.crt"
        for key in ip_map:
            if ip_map[key] == valid_vpns_ipvanish[index]:
                ip_address = key.split(".ovpn")[0]
    vpn_params = dict()
    vpn_params['config'] = vpn_config
    vpn_params['auth'] = auth_file
    vpn_params['cert'] = cert
    vpn_params['ip-address'] = ip_address
    return vpn_params


def get_vpns(country_list):
    import subprocess
    logging.info("Selecting VPNs for experiments.")
    vpns = dict()
    for c in country_list:
        launchable = False
        while not launchable:
            vpns[c] = find_matching_vpn(c.lower())
            logging.info('[%s] Launching VPN (ip: %s, cert: %s, auth: %s, config: %s).'
                         % (c, vpns[c]['ip-address'], vpns[c]['cert'], vpns[c]['auth'], vpns[c]['config']))
            if launch_vpn(vpns[c])[0] < 1:
                launchable = False
                logging.info('[%s] Launching VPN (ip: %s, cert: %s, auth: %s, config: %s) failed. Trying another VPN.'
                                % (c, vpns[c]['ip-address'], vpns[c]['cert'], vpns[c]['auth'], vpns[c]['config']))

            else:
                launchable = True
                logging.info('[%s] Closing VPN (ip: %s, cert: %s, auth: %s, config: %s). '
                             % (c, vpns[c]['ip-address'], vpns[c]['cert'], vpns[c]['auth'], vpns[c]['config']))
                stop_vpn(vpns[c])
                command = ['sudo', 'pkill', '-9', 'openvpn']
                subprocess.Popen(command)
    return vpns


def launch_vpn(vpn_params):
    import vpn.openvpn as openvpn
    import time
    if vpn_params is None:
        logging.info('Empty VPN parameters. Could not start VPN')
        return -1, None
    vpn = openvpn.OpenVPN(timeout=60, config_file=vpn_params['config'], auth_file=vpn_params['auth'],
                          crt_file=vpn_params['cert'])
    logging.info('Launching VPN (ip: %s, cert: %s, auth: %s, config: %s)' % (vpn_params['ip-address'],
                                                                             vpn_params['cert'], vpn_params['auth'],
                                                                             vpn_params['config']))
    vpn.start()
    logging.info('VPN Started: %s, VPN Error: %s, VPN Stopped: %s, VPN Notifications: %s', str(vpn.started),
                 str(vpn.error), str(vpn.stopped), str(vpn.notifications))
    time.sleep(5)
    if not vpn.started:
        logging.info('Error starting VPN')
        return -1, None
    return 1, vpn


def stop_vpn(vpn):
    import subprocess
    command = ['sudo', 'pkill', '-9', 'openvpn']
    subprocess.Popen(command)
