'''

Functions that modify the profile used by our driver go here.
Add firefox profile preferences here.

Avoid modifying default options.

'''

import settings
import utils
from selenium import webdriver
import logging

class WebdriverFirefoxProfile:

    def __init__(self):
        self.profile = webdriver.FirefoxProfile()
        self.profile_path = self.profile.path + "/"
        self.profile.set_preference("app.update.enabled", False)
        self.profile.set_preference("webdriver.log.driver", "ALL")
        self.profile.set_preference("webdriver.log.driver.level", "ALL")
        self.profile.set_preference("webdriver.log.browser.level", "ALL")
        self.profile.set_preference("network.prefetch-next", False)
        self.profile.set_preference("browser.sessionhistory.max_total_viewers", 0)

    def enable_proxy(self, address="127.0.0.1", port=9050):
        logging.info("Enabling proxy on %s and port %s" % (address, str(port)))
        self.profile.set_preference('network.proxy.type', 1)
        self.profile.set_preference('network.proxy.socks', address)
        self.profile.set_preference('network.proxy.socks_port', int(port))

    def disable_cache(self):
        logging.info("Disabling browser-side caching")
        self.profile.set_preference("browser.cache.disk.enable", False)
        self.profile.set_preference("browser.cache.memory.enable", False)
        self.profile.set_preference("browser.cache.offline.enable", False)
        self.profile.set_preference("network.http.use-cache", False)
        self.profile.set_preference("browser.cache.disk_cache_ssl", False)

    def enable_firebug(self):
        logging.info("Enabling the Firebug add-on")
        self.profile.add_extension(settings.FIREBUG_PATH)
        self.profile.set_preference("extensions.firebug.currentVersion", "2.0.8")
        self.profile.set_preference("extensions.firebug.allPagesActivation", "on")
        self.profile.set_preference("extensions.firebug.defaultPanelName", "net")
        self.profile.set_preference("extensions.firebug.net.enableSites", True)
        self.profile.set_preference("extensions.firebug.delayLoad", False)
        self.profile.set_preference("extensions.firebug.onByDefault", True)
        self.profile.set_preference("extensions.firebug.showFirstRunPage", False)

    def enable_netexport(self, har_capture_path):
        logging.info("Enabling the NetExport add-on")
        self.profile.add_extension(settings.NETEXPORT_PATH)
        self.profile.set_preference("extensions.firebug.netexport.alwaysEnableAutoExport", True)
        utils.create_folder(har_capture_path)
        self.profile.set_preference("extensions.firebug.netexport.defaultLogDir", har_capture_path)

    def enable_adblock(self):
        logging.info("Enabling the AdBlock Plus add-on")
        self.profile.add_extension(settings.ADBLOCK_PATH)
        self.profile.set_preference('extensions.adblockplus.suppress_first_run_page', True)
        self.profile.set_preference('extensions.adblockplus.subscriptions_exceptionsurl', '')
        self.profile.set_preference('extensions.adblockplus.subscriptions_listurl', '')
        self.profile.set_preference('extensions.adblockplus.subscriptions_fallbackurl', '')
        self.profile.set_preference('extensions.adblockplus.subscriptions_antiadblockurl', '')
        self.profile.set_preference('extensions.adblockplus.suppress_first_run_page', True)
        self.profile.set_preference('extensions.adblockplus.notificationurl', '')
        self.profile.set_preference('extensions.adblockplus.please_kill_startup_performance', True)
        self.profile.set_preference('extensions.adblockplus.subscriptions_autoupdate', False)

    def enable_ghostery(self):
        logging.info("Enabling the Ghostery add-on")
        self.profile.add_extension(settings.GHOSTERY_PATH)
        import os, shutil
        os.makedirs(self.profile_path+'jetpack/firefox@ghostery.com/simple-storage/')
        dst = os.path.join(self.profile_path,'jetpack/firefox@ghostery.com/simple-storage/store.json')
        shutil.copy(settings.GHOSTERY_PREFS,dst)

    def enable_privacy_badger(self):
        logging.info("Enabling the Privacy Badger add-on")
        self.profile.add_extension(settings.PRIVACY_BADGER_PATH)
        self.profile.set_preference("extensions.privacybadger.currentVersion", "1.0.1");
        self.profile.set_preference('extensions.privacybadger.suppress_first_run_page', True)
        self.profile.set_preference("extensions.privacybadger.enabled", True);
        self.profile.set_preference("extensions.privacybadger.subscriptions_autoupdate", True);
        self.profile.set_preference("extensions.privacybadger.subscriptions_listurl", "https://privacybadger.org/subscriptions2.xml");
        self.profile.set_preference("extensions.privacybadger.subscriptions_fallbackurl", "https://privacybadger.org/getSubscription?version=%VERSION%&url=%SUBSCRIPTION%&downloadURL=%URL%&error=%ERROR%&channelStatus=%CHANNELSTATUS%&responseStatus=%RESPONSESTATUS%");
        self.profile.set_preference("extensions.privacybadger.subscriptions_fallbackerrors", 5);
        self.profile.set_preference("extensions.privacybadger.subscriptions_exceptionsurl", "https://easylist-downloads.privacybadger.org/exceptionrules.txt");

    def enable_noscript(self):
        logging.info("Enabling the NoScript add-on")
        self.profile.add_extension(settings.NOSCRIPT_PATH)
        self.profile.set_preference('noscript.firstRunRedirection', False)
        self.profile.set_preference("capability.policy.maonoscript.javascript.enabled", "allAccess")
        self.profile.set_preference("capability.policy.maonoscript.sites", "about:chrome:resource:")
        self.profile.set_preference("noscript.ABE.enabled", False)
        self.profile.set_preference("noscript.ABE.notify", False)
        self.profile.set_preference("noscript.ABE.wanIpAsLocal", False)
        self.profile.set_preference("noscript.confirmUnblock", False)
        self.profile.set_preference("noscript.contentBlocker", True)
        self.profile.set_preference("noscript.default", "about: chrome:resources:")
        self.profile.set_preference("noscript.firstRunRedirection", False)
        self.profile.set_preference("noscript.global", True)
        self.profile.set_preference("noscript.gtemp", "")
        self.profile.set_preference("noscript.opacizeObject", 3)
        self.profile.set_preference("noscript.forbidWebGL", True)
        self.profile.set_preference("noscript.forbidFonts", False)
        self.profile.set_preference("noscript.options.tabSelectedIndexes", "5,0,0")
        self.profile.set_preference("noscript.policynames", "")
        self.profile.set_preference("noscript.secureCookies", True)
        self.profile.set_preference("noscript.showAllowPage", False)
        self.profile.set_preference("noscript.showBaseDomain", False)
        self.profile.set_preference("noscript.showDistrust", False)
        self.profile.set_preference("noscript.showRecentlyBlocked", False)
        self.profile.set_preference("noscript.showTemp", False)
        self.profile.set_preference("noscript.showTempToPerm", False)
        self.profile.set_preference("noscript.showUntrusted", False)
        self.profile.set_preference("noscript.STS.enabled", False)
        self.profile.set_preference("noscript.subscription.lastCheck", -142148139)
        self.profile.set_preference("noscript.temp", "")
        self.profile.set_preference("noscript.untrusted", "")
        self.profile.set_preference("noscript.forbidMedia", True)
        self.profile.set_preference("noscript.forbidFlash", True)
        self.profile.set_preference("noscript.forbidSilverlight", True)
        self.profile.set_preference("noscript.forbidJava", True)
        self.profile.set_preference("noscript.forbidPlugins", True)
        self.profile.set_preference("noscript.showPermanent", False)
        self.profile.set_preference("noscript.showTempAllowPage", True)
        self.profile.set_preference("noscript.showRevokeTemp", True)
        self.profile.set_preference("noscript.notify", False)
        self.profile.set_preference("noscript.autoReload", False)
        self.profile.set_preference("noscript.autoReload.allTabs", False)
        self.profile.set_preference("noscript.cascadePermissions", True)
        self.profile.set_preference("noscript.restrictSubdocScripting", True)
        self.profile.set_preference("noscript.ABE.migration", 1)
        self.profile.set_preference("noscript.cascadePermissions", False)
        self.profile.set_preference("noscript.forbidBookmarklets", True)
        self.profile.set_preference("noscript.forbidMetaRefresh", True)
        self.profile.set_preference("noscript.global", False)
        self.profile.set_preference("noscript.nselForce", False)
        self.profile.set_preference("noscript.nselNever", True)
        self.profile.set_preference("noscript.options.tabSelectedIndexes", "5,5,0")
        self.profile.set_preference("noscript.subscription.lastCheck", 1137677561)
        self.profile.set_preference("noscript.version", "2.6.9.2")

    def get_profile(self):
        return self.profile
