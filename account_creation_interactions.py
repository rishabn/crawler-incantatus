'''

Functions dealing with login/authentication interactions go in this class.
Add new templates for authentication interactions here.

'''

import logging
import settings
import dom_interactions
import authentication_interactions
import time

class AccountCreation:

    def __init__(self, driver):
        self.driver = driver
        self.di = dom_interactions.DomInteractions(driver)
        self.facebook_compatible = self.__check_facebook_signup_compatibility()
        self.signup_compatible = self.__check_signup_compatibility()
        self.interactions = list()

    def __check_facebook_signup_compatibility(self):
        logging.debug("Checking compatibility of URL for facebook-based signups")
        rules = list()
        keywords = ["signupwithfacebook"]
        tags = ["a", "input", "button"]
        exclusions = ["hidden"]
        for t in tags:
            rules.append({'tag': t, 'keywords': keywords, 'exclusions': exclusions})
        status = self.di.does_page_satisfy_rules(rules)
        if True in status:
            return True
        else:
            return False

    def __check_signup_compatibility(self):
        logging.debug("Checking compatibility of URL for auto-signup")
        rules = list()
        keywords = ["signup"]
        tags = ["a", "input", "button"]
        exclusions = ["hidden", "next"]
        for t in tags:
            rules.append({'tag': t, 'keywords': keywords, 'exclusions': exclusions})
        status = self.di.does_page_satisfy_rules(rules)
        if True in status:
            return True
        else:
            return False

    def __base_interaction(self):
        logging.debug("Clicking element that is most similar to a sign in link")
        rules = list()
        keywords = ["signup"]
        tags = ["a", "input", "button"]
        exclusions = ["hidden", "next"]
        for t in tags:
            rules.append({'tag': t, 'keywords': keywords, 'exclusions': exclusions})
        clicked_element_attributes = self.di.click_closest_satisfying_element(rules)
        if clicked_element_attributes is not None:
            self.interactions.append({'interaction':"click", 'element': clicked_element_attributes})

    def __complete_facebook_signup(self, credentials):
        rules = list()
        status = False
        keywords, tags, exclusions = ["signupwithfacebook", "continuewithfacebook"], ["a", "input", "button"], ["hidden"]
        for t in tags:
            rules.append({'tag': t, 'keywords': keywords, 'exclusions': exclusions})
        main_window_handle = None
        while not main_window_handle:
            main_window_handle = self.driver.current_window_handle
        clicked_element_attributes = self.di.click_closest_satisfying_element(rules)
        if clicked_element_attributes is None:
            status = False
            return status
        else:
            self.interactions.append({'interaction':"click", 'element': clicked_element_attributes})
        signin_window_handle = None
        if len(self.driver.window_handles) > 1:
            logging.info("Found a popup window")
            while not signin_window_handle:
                for handle in self.driver.window_handles:
                    if handle != main_window_handle:
                        signin_window_handle = handle
                        break
            if signin_window_handle:
                self.driver.switch_to.window(signin_window_handle)
                self.interactions.append({'interaction':"switch-to-popup", 'element': None})
        else:
            logging.info("No popup window found")
            self.driver.switch_to.window(main_window_handle)
        ai = authentication_interactions.AuthenticationInteractions(self.driver)
        status = ai.single_page_signin(credentials)
        for i in ai.interactions:
            self.interactions.append(i)
        self.driver.switch_to.window(main_window_handle)
        self.interactions.append({'interaction':"switch-to-popup", 'element': None})
        # If there are more popups, one of them is the permissions popup
        while len(self.driver.window_handles) > 1:
            time.sleep(.5)
            permissions_window_handle = None
            while not permissions_window_handle:
                for handle in self.driver.window_handles:
                    if handle != main_window_handle:
                        permissions_window_handle = handle
                        break
            if permissions_window_handle:
                self.driver.switch_to.window(permissions_window_handle)
                self.interactions.append({'interaction': "switch-to-popup", 'element': None})
                keywords, tags, exclusions = ["okay", "continue"], ["a", "input", "button"], ["hidden"]
                for t in tags:
                    rules.append({'tag': t, 'keywords': keywords, 'exclusions': exclusions})
                clicked_element_attributes = self.di.click_closest_satisfying_element(rules)
                if clicked_element_attributes is None:
                    status = False
                    return status
                else:
                    self.interactions.append({'interaction':"click", 'element': clicked_element_attributes})
            self.driver.switch_to.window(main_window_handle)
            self.interactions.append({'interaction': "switch-to-popup", 'element': None})
        return status

    def record_account_creation_interactions(self, credentials):
        if "facebook" in credentials['user'].split("|")[0]:
            credentials['user'] = credentials['user'].split("|")[1]
        logging.debug("Recording interactions required to perform an auto-login")
        logged_in = False
        if not self.signup_compatible:
            logging.info("Unable to automatically signup for this site (couldn't find the signup button/link)")
            return logged_in
        self.__base_interaction()
        current_interactions = len(self.interactions)
        while (not logged_in) and (current_interactions <= settings.DRIVER_ACCOUNT_CREATION_MAX_INTERACTIONS):
            current_interactions = len(self.interactions)
            if self.__check_facebook_signup_compatibility():
                logging.info("Found a facebook signup form")
                self.__complete_facebook_signup(credentials)
                logged_in = True
        logging.debug("Login status: %s " % logged_in)
        return logged_in
