'''

Functions that parse page elements and DOM objects go here

'''

import logging
import utils
import time
import settings
from selenium.common.exceptions import WebDriverException,StaleElementReferenceException, ElementNotSelectableException, NoSuchElementException, ElementNotVisibleException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains


class DomInteractions:

    def __init__(self, driver):
        self.driver = driver

    def __element_is_hidden(self, element):
        logging.debug("Checking if element is hidden")
        try:
            record = self.__get_element_attributes(element, "")
        except (StaleElementReferenceException, ElementNotSelectableException, NoSuchElementException, ElementNotVisibleException, TypeError) as ex:
            logging.debug("Exception (%s) while checking if element is hidden" % ex)
            return True
        try:
            if record['displayed'] == unicode(False):
                return True
            if record['enabled'] == unicode(False):
                return True
        except TypeError:
            return True
        return False

    def __element_is_excluded(self, element, exclusion_keywords):
        logging.debug("Checking if element is to be excluded")
        try:
            record = self.__get_element_attributes(element, "")
        except (StaleElementReferenceException, ElementNotSelectableException, NoSuchElementException, ElementNotVisibleException, TypeError) as ex:
            logging.debug("Exception (%s) while checking if element is excluded" % ex)
            return True
        try:
            for keyword in exclusion_keywords:
                if keyword in record['key-string']:
                    return True
        except TypeError:
            return True
        return False

    def __element_contains_keywords(self, element, keywords):
        logging.debug("Checking if element contains some specific keywords")
        try:
            record = self.__get_element_attributes(element, "")
        except (StaleElementReferenceException, ElementNotSelectableException, NoSuchElementException, ElementNotVisibleException, TypeError) as ex:
            logging.debug("Exception (%s) while checking if element contains keywords" % ex)
            return False
        for attribute in ["value", "text", "type", "id"]:
            try:
                element_keyword = record[attribute].lower().strip().replace(" ","")
            except Exception as ex:
                logging.debug("Exception (%s) while formatting element attribute." % ex)
                element_keyword = unicode("")
            for keyword in keywords:
                logging.debug("Looking for Keyword: %s in Attribute: %s (element keyword: %s)" % (keyword, attribute, element_keyword))
                if keyword in element_keyword:
                    logging.debug("Found Keyword: %s in Attribute: %s (element keyword: %s)" % (keyword, attribute, element_keyword))
                    return True
        return False

    def __element_matches_keywords(self, element, keywords):
        logging.debug("Checking if element has some exact keywords")
        try:
            record = self.__get_element_attributes(element, "")
        except (StaleElementReferenceException, ElementNotSelectableException, NoSuchElementException, ElementNotVisibleException, TypeError) as ex:
            logging.debug("Exception (%s) while checking if element matches keywords" % ex)
            return False
        for attribute in ["value", "text", "type", "id"]:
            try:
                element_keyword = record[attribute].lower().strip().replace(" ","")
            except Exception as ex:
                logging.debug("Exception (%s) while formatting element attribute." % ex)
                element_keyword = unicode("")
            if element_keyword in keywords:
                return True
        return False

    @staticmethod
    def __get_element_attributes(element, tag):
        if element is None:
            logging.debug("Element is None element")
            return None
        else:
            try:
                te, record = element, {}
                record['tag'] = tag
                record['title'] = unicode(te.get_attribute("title"))
                record['type'] = unicode(te.get_attribute("type"))
                record['label'] = unicode(te.get_attribute("label"))
                record['value'] = unicode(te.get_attribute("value"))
                record['id'] = unicode(te.get_attribute("id"))
                record['name'] = unicode(te.get_attribute("name"))
                record['aria'] = unicode(te.get_attribute("aria-label"))
                record['maxlen'] = unicode(te.get_attribute("maxlength"))
                record['disabled'] = unicode(te.get_attribute("disabled"))
                record['displayed'] = unicode(te.is_displayed())
                record['enabled'] = unicode(te.is_enabled())
                record['href'] = unicode(te.get_attribute("href"))
                record['autofocus'] = unicode(te.get_attribute("autofocus"))
                record['text'] = unicode(te.text)
                record['string'] = (record['title'].split("\n")[0] + record['type'].split("\n")[0] + record['label'].split("\n")[0] + record['value'].split("\n")[0]).lower()
                record['string'] += (record['id'].split("\n")[0] + record['aria'].split("\n")[0] + record['name'].split("\n")[0] + record['text'].split("\n")[0]).lower()
                record['key-string'] = record['text'].split("\n")[0] + ", \t" + record['name'].split("\n")[0] + ", \t" + record['id'].split("\n")[0]
                record['key-string'] += ", \t" + record['aria'].split("\n")[0] + ", \t" + record['label'].split("\n")[0] + ", \t" + record['tag']
                return record
            except (StaleElementReferenceException, ElementNotVisibleException, ElementNotSelectableException, NoSuchElementException, Exception) as ex:
                logging.debug("Exception (%s) while getting element attributes" % ex)
                return None

    def scroll_to_bottom(self):
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(1)

    def scroll_to_top(self):
        self.driver.execute_script("window.scrollTo(0, 0);")
        time.sleep(1)

    def click_longest_textlink(self):
        logging.debug("Getting link with the longest text attribute")
        longest_link, link = 0, None
        all_links = self.driver.find_elements_by_tag_name("a")
        for e in all_links:
            try:
                text_len = len(unicode(e.text))
                if text_len > longest_link:
                    longest_link = text_len
                    link = e
            except(StaleElementReferenceException, ElementNotSelectableException, ElementNotVisibleException, Exception) as ex:
                logging.debug("Exception (%s) while clicking longest link" % ex)
        try:
            ActionChains(self.driver).move_to_element(link).click().perform()
            time.sleep(1)
        except Exception as ex:
            logging.debug("Exception occurred while clicking on element (%s). Trying a non action-chain approach." % ex)
            link.click()

    def click_shortest_link_containing_string(self, string):
        shortest_link_text, link = 0, None
        all_links = self.driver.find_elements_by_tag_name("a")
        for e in all_links:
            try:
                link_text = unicode(e.text)
                if string.lower() in link_text.lower():
                    if len(link_text) < len(shortest_link_text):
                        shortest_link_text = e.text
                        link = e
            except(StaleElementReferenceException, ElementNotSelectableException, ElementNotVisibleException, Exception) as ex:
                logging.debug("Exception (%s) while clicking longest link" % ex)
        try:
            ActionChains(self.driver).move_to_element(link).click().perform()
            time.sleep(1)
        except Exception as ex:
            logging.debug("Exception occurred while clicking on element (%s). Trying a non action-chain approach." % ex)
            link.click()

    def get_all_input_elements(self):
        logging.debug("Getting all available input elements on page")
        all_input_elements = self.driver.find_elements_by_tag_name("input")
        el_attributes = list()
        for el in all_input_elements:
            attributes = dict()
            attributes['title'] = unicode(el.get_attribute("title"))
            attributes['type'] = unicode(el.get_attribute("type"))
            attributes['label'] = unicode(el.get_attribute("label"))
            attributes['id'] = unicode(el.get_attribute("id"))
            attributes['name'] = unicode(el.get_attribute("name"))
            attributes['aria-label'] = unicode(el.get_attribute("aria-label"))
            attributes['max-length'] = unicode(el.get_attribute("maxlength"))
            attributes['style'] = unicode(el.get_attribute("style"))
            if (attributes not in el_attributes) and (attributes['type'] != "hidden"):
                el_attributes.append(attributes)
        return el_attributes

    def does_page_satisfy_rules(self, rules):
        logging.debug("Checking if the current URL satisfies certain rules")
        # rules = [{'tag': t, 'keywords': [kw1, ...], 'exclusions': [ex1, ...]}]
        rules_status = [False] * len(rules)
        rule_index = 0
        for rule in rules:
            elements = self.driver.find_elements_by_tag_name(rule['tag'])
            logging.debug("There are %d elements with Tag %s" % (len(elements), rule['tag']))
            for e in elements:
                if rules_status[rule_index] or self.__element_is_hidden(e) or self.__element_is_excluded(e, rule['exclusions']):
                    continue
                if self.__element_contains_keywords(e, rule['keywords']):
                    rules_status[rule_index] = True
                    continue
            rule_index += 1
        return rules_status

    def click_closest_satisfying_element(self, rules):
        logging.debug("Clicking the element that best satisfies the input rules")
        # rules = [{'tag': t, 'keywords': [kw1, ...], 'exclusions': [ex1, ...]}]
        satisfying_element = None
        closest_element, len_closest_element = None, 999
        for rule in rules:
            elements = self.driver.find_elements_by_tag_name(rule['tag'])
            logging.debug("There are %d elements with Tag %s" % (len(elements), rule['tag']))
            for e in elements:
                if self.__element_is_excluded(e, rule['exclusions']) or self.__element_is_hidden(e):
                    continue
                if self.__element_matches_keywords(e, rule['keywords']):
                    satisfying_element = e
                    break
                if self.__element_contains_keywords(e, rule['keywords']):
                    try:
                        record = self.__get_element_attributes(e, "")
                        if len(record['id']) == 0:
                            continue
                        if len(record['text'].lower().strip().replace(" ", "")) < len_closest_element:
                            try:
                                len_closest_element = len(record['text'].lower().strip().replace(" ", ""))
                                closest_element = e
                            except Exception as ex:
                                logging.debug("Exception (%s) while getting element text length" % ex)
                    except TypeError as ex:
                        logging.debug("Exception (%s) while clicking closest element" % ex)
        if satisfying_element is not None:
            try:
                record = self.__get_element_attributes(satisfying_element, "")
                logging.debug("Found an exact match element: %s" % utils.dict_to_json(record))
                try:
                    ActionChains(self.driver).move_to_element(satisfying_element).click().perform()
                    time.sleep(1)
                except Exception as ex:
                    logging.debug("Exception occurred while clicking on div element (%s). Trying a non-action-chain approach." % ex)
                    satisfying_element.click()
                time.sleep(1)
                return record
            except (WebDriverException, StaleElementReferenceException, ElementNotSelectableException, ElementNotVisibleException) as ex:
                logging.debug("Exception (%s) while clicking satisfying element" % ex)
        elif closest_element is not None:
            try:
                record = self.__get_element_attributes(closest_element, "")
                logging.debug("Found a nearest match element: %s" % utils.dict_to_json(record))
                try:
                        ActionChains(self.driver).move_to_element(closest_element).click().perform()
                        time.sleep(1)
                except Exception as ex:
                    logging.debug("Exception occurred while clicking on element (%s). Trying a non action-chain approach." % ex)
                    closest_element.click()
                time.sleep(1)
                return record
            except (WebDriverException, StaleElementReferenceException, ElementNotVisibleException, ElementNotSelectableException) as ex:
                logging.debug("Exception (%s) while clicking closest element" % ex)
        else:
            logging.debug("No suitable element found")
            return None

    def send_text_to_all_rule_satisfying_elements(self, rules, text, max_count=0):
        logging.debug("Sending text: %s to all elements that meet certain requirements" % text)
        # rules = [{'tag': t, 'keywords': [kw1, ...], 'exclusions': [ex1, ...]}]
        current_count = 0
        satisfying_elements = list()
        for rule in rules:
            elements = self.driver.find_elements_by_tag_name(rule['tag'])
            logging.debug("There are %d elements with Tag %s" % (len(elements), rule['tag']))
            for e in elements:
                if (current_count >= max_count) and (max_count > 0):
                    logging.debug("Already filled out Max (%d) fields. Breaking." % current_count)
                    break
                if self.__element_is_excluded(e, rule['exclusions']) or self.__element_is_hidden(e):
                    continue
                if not self.__element_contains_keywords(e, rule['keywords']):
                    continue
                try:
                    record = self.__get_element_attributes(e, rule['tag'])
                    if len(record['id']) == 0:
                        continue
                    satisfying_elements.append(record)
                    current_count += 1
                    logging.debug("Current number of fields filled: %d | Max: %d" % (current_count, max_count))
                    try:
                        if "div" in rule['tag']:
                            ActionChains(self.driver).move_to_element(e).click().perform()
                            time.sleep(1)
                    except Exception as ex:
                        logging.debug("Exception occurred while clicking on div element (%s)" % ex)
                    e.send_keys(text)
                except (StaleElementReferenceException, ElementNotSelectableException, ElementNotVisibleException, NoSuchElementException) as ex:
                    logging.debug("Exception (%s) while sending text (%s) to element " % (ex, text))
        if len(satisfying_elements) == 0:
            return None
        return satisfying_elements

    def click_element_with_id(self, element_id):
        try:
            wait = WebDriverWait(self.driver, settings.DRIVER_MAX_PAGE_LOAD_TIME)
            element = wait.until(EC.element_to_be_clickable((By.ID,element_id)))
            element.click()
            return True
        except TimeoutException as ex:
            logging.info("Could not locate the element with ID: %s that was to be clicked. " % element_id)
            return False

    def send_text_to_element_with_id(self, text, element_id):
        try:
            wait = WebDriverWait(self.driver, settings.DRIVER_MAX_PAGE_LOAD_TIME)
            element = wait.until(EC.visibility_of_element_located((By.ID, element_id)))
            element.send_keys(text)
            return True
        except TimeoutException as ex:
            logging.info("Could not locate the element with ID: %s that needs to receive text. " % element_id)
            return False
