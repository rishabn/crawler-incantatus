'''

__author__ = "Rishab Nithyanand"
__email__ = "rnithyanand@cs.stonybrook.edu"

Core functions (page loads + actions) belong in this class.

'''

import logging
from httplib import CannotSendRequest, BadStatusLine

from selenium.common.exceptions import TimeoutException, UnexpectedAlertPresentException, NoAlertPresentException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import account_creation_interactions
import authentication_interactions
import crawler_manager
import posting_interactions
import replay_interactions
import search_interactions
import misc_interactions
import dom_interactions

import settings
import utils
from utils import FunctionTimeoutError, timeout


class CrawlerIncantatus:

    def __init__(self, params):
        self.params = params
        self.__default_setup()

    def __default_setup(self):
        self.cm = crawler_manager.CrawlerManager(self.params)
        self.cm.initialize_default_setup()
        self.crawler_state = self.cm.get_crawler_state()
        self.driver = self.crawler_state["driver"]

    @staticmethod
    def __sanitize_url(url):
        if "http" not in url.split("/")[0]:
            url = "http://" + url
        return url

    def __handle_alerts(self):
        logging.debug("Checking loaded page for alerts.")
        try:
            WebDriverWait(self.driver, settings.DRIVER_ALERT_WAIT_TIME).until(EC.alert_is_present())
            logging.debug("Alert found on page. Trying to dismiss alert.")
            alert = self.driver.switch_to_alert()
            alert.dismiss()
            logging.debug("Checking to see if alert is still present.")
            try:
                self.driver.switch_to_alert()
                raise UnexpectedAlertPresentException
            except NoAlertPresentException:
                logging.debug("Alert was dismissed successfully.")
                handled = True
                return handled
            except UnexpectedAlertPresentException:
                logging.debug("Alert could not be dismissed. Alert handler failed.")
                handled = False
                return handled
        except (NoAlertPresentException, TimeoutException):
            logging.debug("No alert found on page.")
            handled = True
            return handled
        except (FunctionTimeoutError, BadStatusLine) as ex:
            logging.debug("Exception (%s) occurred while checking page for alerts. " % ex)
            handled = False
            return handled

    def __handle_popups(self):
        try:
            windows = self.driver.window_handles
            if len(windows) == 1:
                logging.debug("Found no popups.")
                handled = True
                return handled
            else:
                logging.debug("Found a popup. We don't know how to deal with these, yet. :(")
                handled = False
                return handled
        except (FunctionTimeoutError, TimeoutException, CannotSendRequest, BadStatusLine) as ex:
            logging.debug("Exception (%s) while checking for popups." % ex)
            handled = False
            return handled

    @timeout(settings.FUNCTION_TIMEOUT)
    def load_page(self, url):
        url = self.__sanitize_url(url)
        logging.info("Loading URL: %s" % url)
        alert_handler_status, popup_handler_status = False, False
        try:
            if settings.DRIVER_DELETE_COOKIES:
                logging.debug("Deleting all cookies.")
                self.driver.delete_all_cookies()
        except Exception as ex:
            logging.info("Exception (%s) occurred while deleting cookies." % ex)
        try:
            self.driver.get(url)
            logging.info("Loaded page: %s" % url)
            load_status = True
            alert_handler_status = self.__handle_alerts()
            popup_handler_status = self.__handle_popups()
        except (TimeoutException, UnexpectedAlertPresentException) as ex:
            logging.info("Exception (%s) while loading page: %s" % (ex, url))
            load_status = False
        except FunctionTimeoutError:
            logging.info("FunctionTimeoutError occurred. ")
            load_status, alert_handler_status, popup_handler_status = False, False, False
        except Exception as ex:
            logging.info("Exception %s occurred while loading page: %s" % (ex, url))
            load_status = False
        status = {'load': load_status, 'alert': alert_handler_status, 'popup': popup_handler_status}
        logging.info("Status of [ Load | AlertHandler | PopupHandler ]: [ %s | %s | %s ]" % (status['load'], status['alert'], status['popup']))
        return status

    @timeout(settings.FUNCTION_TIMEOUT)
    def scroll_to_bottom_and_back(self):
        try:
            mi = misc_interactions.MiscInteractions(self.driver)
            mi.scroll_to_bottom_and_back()
        except (FunctionTimeoutError, TimeoutException) as ex:
            logging.info("Timeout exception (%s) while scrolling page" % ex)

    @timeout(settings.FUNCTION_TIMEOUT)
    def click_longest_link(self):
        try:
            mi = misc_interactions.MiscInteractions(self.driver)
            mi.click_longest_text_link()
        except (FunctionTimeoutError, TimeoutException, Exception) as ex:
            logging.info("Timeout exception (%s) while clicking longest link" % ex)

    @timeout(settings.FUNCTION_TIMEOUT)
    def click_shortest_link_with_rules(self, rules):
        # TODO: Implement this using the \"rules\" for finding links
        di = dom_interactions.DomInteractions(self.driver)
        click_status = None
        page_status = di.does_page_satisfy_rules(rules)[0]
        if page_status is True:
            logging.info("This page has a link satisfying the given rules.")
            click_status = di.click_closest_satisfying_element(rules)
        return click_status

    @timeout(settings.FUNCTION_TIMEOUT)
    def record_search_rule(self):
        status = {'search-element': None, 'search-rule': None}
        try:
            si = search_interactions.SearchInteractions(self.driver)
            si.get_search_interactions()
            logging.info("Search Element | Rule: [ %s | %s ]" % (si.search_element, si.search_rule))
            status['search-element'], status['search-rule'] = si.search_element, si.search_rule
            interaction = si.interactions
        except (FunctionTimeoutError, TimeoutException) as ex:
            logging.info("Timeout Exception (%s) while finding search rule." % ex)
            status['search-element'], status['search-rule'] = None, "TimedOut"
            interaction = None
        return interaction

    @timeout(3*settings.FUNCTION_TIMEOUT)
    def record_login_rules(self, url):
        interactions = list()
        try:
            ai = authentication_interactions.AuthenticationInteractions(self.driver)
            logging.info("Login compatibility: %s" % ai.compatible)
            credentials = utils.read_credentials_from_file(settings.CREDENTIALS_PATH, url)
            if (credentials['user'] is None) or (credentials['pass'] is None):
                logging.info("Could not find credentials for URL: %s" % url)
                return list()
            status = ai.record_login_interaction(credentials)
            if status is False:
                logging.info("Failed to login.")
                interactions = list()
            else:
                logging.info("Logged in with %d interactions." % len(ai.interactions))
                interactions = ai.interactions
        except (FunctionTimeoutError, TimeoutException) as ex:
            logging.info("Exception (%s) while recording login interactions." % ex)
        return interactions

    @timeout(5*settings.FUNCTION_TIMEOUT)
    def record_message_posting_rules(self, url):
        interactions = list()
        try:
            ai = authentication_interactions.AuthenticationInteractions(self.driver)
            logging.info("Login compatibility: %s" % ai.compatible)
            credentials = utils.read_credentials_from_file(settings.CREDENTIALS_PATH, url)
            if (credentials['user'] is None) or (credentials['pass'] is None):
                logging.info("Could not find credentials for URL: %s" % url)
                return list()
            status = ai.record_login_interaction(credentials)
            if status is False:
                logging.info("Failed to login.")
                interactions = list()
            else:
                logging.info("Logged in with %d interactions." % len(ai.interactions))
                a_interactions = ai.interactions
                pi = posting_interactions.PostingInteractions(self.driver)
                if pi.compatible:
                    logging.info("URL is compatible with posting.")
                    status = pi.make_post("testing")
                    logging.info("Made post")
                if status:
                    p_interactions = pi.interactions
                    interactions = a_interactions + p_interactions
                    logging.info("Completed posting in %d interactions" % len(interactions))
                else:
                    interactions = list()
        except (FunctionTimeoutError, TimeoutException) as ex:
            logging.info("Exception (%s) while recording login interactions." % ex)
        return interactions

    @timeout(3*settings.FUNCTION_TIMEOUT)
    def record_account_creation_rules(self, url):
        interactions = list()
        try:
            ac = account_creation_interactions.AccountCreation(self.driver)
            logging.info("Account creation compatibility: %s" % ac.signup_compatible)
            credentials = utils.read_credentials_from_file(settings.CREDENTIALS_PATH, url)
            if (credentials['user'] is None) or (credentials['pass'] is None):
                logging.info("Could not find credentials for URL: %s" % url)
                return list()
            status = ac.record_account_creation_interactions(credentials)
            if status is False:
                logging.info("Failed to create account.")
                interactions = list()
            else:
                logging.info("Logged in with %d interactions." % len(ac.interactions))
                interactions = ac.interactions
        except (FunctionTimeoutError, TimeoutException) as ex:
            logging.info("Exception (%s) while recording login interactions." % ex)
        return interactions

    def __get_log_for_url(self, url):
        log = self.params['replay-log']
        started_replay = False
        log_for_url = list()
        for l in log:
            if l['source'] == "frontpage-load":
                continue
            if (l['url'] != url) and (started_replay is False):
                continue
            elif (l['url'] != url) and (started_replay is True):
                break
            else:
                started_replay = True
                logging.debug("Current interaction URL: %s. Looking for: %s. We have a match." % (l['url'], url))
                log_for_url.append(l)
            logging.debug("Adding interaction record %s for url: %s" % (utils.dict_to_json(l), url))
        return log_for_url

    def get_urls_from_replay_log(self):
        log = self.params['replay-log']
        urls = list()
        for l in log:
            if l["source"] == "frontpage-load":
                continue
            d = {'url': l['url'], 'idx': len(urls)}
            if d not in urls:
                urls.append(d)
        return urls

    def replay_interactions_for_url(self, url):
        log = self.__get_log_for_url(url)
        logging.debug("Interaction records for URL %s : %s " % (url, utils.dict_to_json(log)))
        ri = replay_interactions.ReplayInteractions(self.driver, log)
        return ri.status

    def get_driver_state(self):
        return self.driver

    def restart_crawler(self):
        self.cm.end_crawl()
        self.__default_setup()

    def kill_crawler(self):
        self.cm.end_crawl()

