'''

Functions dealing with social-media + forum posting go here.
If you need to sign-in before posting, this code assumes you've
already signed in (or, loaded the required cookies in you driver
instance).

'''

import logging
import settings
import dom_interactions
import time

class PostingInteractions:

    def __init__(self, driver):
        self.driver = driver
        self.di = dom_interactions.DomInteractions(driver)
        self.text_rules, self.button_rules = list(), list()
        self.compatible = self.__check_posting_compatibility()
        self.interactions = list()

    def __check_posting_compatibility(self):
        text_compatible = False
        self.text_rules.append({'tag': "textarea", 'keywords': [], 'exclusions': ["comment"]})
        self.text_rules.append({'tag': "div", 'keywords': ["textarea", "textbox", "editor-richtext"], 'exclusions': []})
        button_compatible = False
        self.button_rules.append({'tag': "div", 'keywords': ["sharebutton"], 'exclusions': []})
        self.button_rules.append({'tag': "button", 'keywords': ["post", "share", "tweet", "send"], 'exclusions': []})
        if True in self.di.does_page_satisfy_rules(self.text_rules):
            text_compatible = True
        if True in self.di.does_page_satisfy_rules(self.button_rules):
            button_compatible = True
        if text_compatible and button_compatible:
            logging.debug("This URL seems to be compatible for automatic posting.")
            return True
        else:
            logging.debug("This URL does not seem to be compatible with our automatic posting. Textarea: %s | PostButton: %s" % (text_compatible, button_compatible))

    def make_post(self, text):
        if not self.compatible:
            return False
        sent, text_sent, button_clicked = False, False, False
        while (sent is False) and (len(self.interactions) < settings.DRIVER_POST_MAX_INTERACTIONS):
            text_elements = self.di.send_text_to_all_rule_satisfying_elements(self.text_rules, "%s @ %s" % (text, time.time()))
            if text_elements is None:
                logging.debug("Failed while sending message to text element")
                text_sent = False
            else:
                for te in text_elements:
                    if te is None:
                        continue
                    self.interactions.append({'interaction':"type|"+text, 'element': te})
                    text_sent = True
            button = self.di.click_closest_satisfying_element(self.button_rules)
            if button is None:
                logging.debug("Failed while clicking submit button/link")
                button_clicked = False
            else:
                self.interactions.append({'interaction': "click", 'element': button})
                button_clicked = True
            sent = text_sent and button_clicked
        return sent


