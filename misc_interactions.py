import logging
import utils
import dom_interactions
import time


class MiscInteractions:

    def __init__(self, driver):
        self.driver = driver
        self.di = dom_interactions.DomInteractions(driver)

    def scroll_to_bottom_and_back(self):
        self.di.scroll_to_bottom()
        time.sleep(1)
        self.di.scroll_to_top()
        time.sleep(1)

    def click_longest_text_link(self):
        self.di.click_longest_textlink()
        time.sleep(3)

#    def click_shortest_link_containing_string(self, strings):
# TODO
#         time.sleep(3)
