'''
Functions that deal with data-logging go in this class.
Only pre- and post-pageload functions should be exposed.
'''

import time
import settings
import utils
import logging
from httplib import CannotSendRequest, BadStatusLine
import pickle
import os
import signal
from subprocess import Popen


class DataManager:

    def __init__(self, dm_params):
        self.params = dm_params
        self.capture_path = dm_params["capture-path"]
        self.driver = dm_params["driver"]
        self.filename = dm_params["filename"]
        self.pcap_call = None

    @utils.timeout(settings.FUNCTION_TIMEOUT)
    def __record_screenshots(self):
        utils.create_folder(self.capture_path)
        screenshot_path = self.capture_path + "/screenshots/"
        utils.create_folder(screenshot_path)
        recorded, attempts = False, 0
        while (not recorded) and (attempts <= settings.DRIVER_SCREENSHOT_RETRIES):
            try:
                attempts += 1
                self.driver.save_screenshot(screenshot_path + self.filename + ".png")
                recorded = True
                logging.debug("Screenshot saved successfully.")
            except (CannotSendRequest, BadStatusLine) as ex:
                logging.info("CSR/BSL Exception (%s) while saving screenshot. Retrying..." % ex)
            #except (IOError, utils.FunctionTimeoutError) as ex:
            except Exception as ex:
                logging.info("Fatal Exception (%s) while saving screenshot. Aborting." % ex)
                return recorded
        return recorded

    @utils.timeout(settings.FUNCTION_TIMEOUT)
    def __record_html(self):
        utils.create_folder(self.capture_path)
        html_path = self.capture_path + "/html/"
        utils.create_folder(html_path)
        recorded, attempts = False, 0
        f = open(html_path + self.filename + ".html", "w")
        while (not recorded) and (attempts <= settings.DRIVER_HTML_SAVE_RETRIES):
            try:
                attempts += 1
                f.write(self.driver.page_source.encode("UTF-8"))
                recorded = True
                logging.debug("HTML saved successfully.")
            except (CannotSendRequest, BadStatusLine) as ex:
                logging.info("CSR/BSL Exception (%s) while saving html source. Retrying..." % ex)
            #except (IOError, utils.FunctionTimeoutError) as ex:
            except Exception as ex:
                logging.info("Fatal Exception (%s) while saving html source. Aborting." % ex)
                f.close()
                return recorded
        f.close()
        return recorded

    @utils.timeout(settings.FUNCTION_TIMEOUT)
    def __save_cookies(self):
        utils.create_folder(self.capture_path)
        cookie_path = self.capture_path + "/cookies/"
        utils.create_folder(cookie_path)
        recorded, attempts = False, 0
        while (not recorded) and (attempts <= settings.DRIVER_COOKIE_SAVE_RETRIES):
            try:
                attempts += 1
                pickle.dump(self.driver.get_cookies(), open(cookie_path + self.filename + ".pickle", "wb"))
                recorded = True
            except (CannotSendRequest, BadStatusLine) as ex:
                logging.info("CSR/BSL exception (%s) while saving cookies. Retrying..." % ex)
            #except (IOError, utils.FunctionTimeoutError) as ex:
            except Exception as ex:
                logging.info("Fatal exception (%s) while saving cookies. Aborting." % ex)
                return recorded
        return recorded

    @utils.timeout(settings.FUNCTION_TIMEOUT)
    def __start_pcap(self):
        utils.create_folder(self.capture_path)
        pcap_path = self.capture_path + "/pcaps/"
        utils.create_folder(pcap_path)
        filename = pcap_path + self.filename + ".pcap"
        self.pcap_call = Popen(["tshark", "-w", filename, "-i", settings.TSHARK_INTERFACE], preexec_fn=os.setsid)

    @utils.timeout(settings.FUNCTION_TIMEOUT)
    def __stop_pcap(self):
        not_killed, attempts = True, 0
        if self.pcap_call is None:
            return False
        while not_killed and attempts <= settings.TOR_KILL_RETRIES and self.pcap_call is not None:
            try:
                attempts += 1
                pid = self.pcap_call.pid
                os.killpg(os.getpgid(pid), signal.SIGTERM)
                logging.debug("Killed TShark PCAP recording.")
                not_killed = False
            except Exception as ex:
                logging.info("Exception (%s) while killing TShark client. Attempt: %d (of MAX: %d)." %(ex, attempts, settings.TOR_KILL_RETRIES))
        return not_killed


    def pre_load_collection(self):
        logging.debug("Pre-pageload data collection.")
        if settings.TSHARK_RECORDER:
            self.__start_pcap()

    def post_load_collection(self):
        logging.debug("Post-pageload data collection.")
        time.sleep(10)
        self.__record_html()
        self.__record_screenshots()
        self.__save_cookies()
        if settings.TSHARK_RECORDER:
            time.sleep(10)
            self.__stop_pcap()


