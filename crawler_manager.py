'''

Functions that handle building/initialization and termination/teardown of our
crawler components (e.g., driver settings, display settings, etc.) go here.

Avoid modifying default options.

'''

import utils
import profile_manager
import settings
import logging
import time
import signal
import os
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from subprocess import Popen

class CrawlerManager:

    def __init__(self, params):
        utils.create_folder(params["capture-path"])
        self.terminated = False
        self.init_params = params
        self.driver, self.profile, self.display, self.tor_call = None, None, None, None
        self.crawler_state = {"driver": self.driver, "display": self.display, "profile": self.profile, "tor-call": self.tor_call}
        self.vpn = None
        self.open_pids = list()

    def __initialize_default_webdriver(self):
        logging.debug("Initializing default FF driver. DEFAULT: Enabled Cache + Firebug + NetExport + Tor (if specified) ")
        self.pm = profile_manager.WebdriverFirefoxProfile()
        #self.pm.disable_cache()
        if settings.ENABLE_FIREBUG:
            self.pm.enable_firebug()
        if settings.ENABLE_NETEXPORT:
            self.pm.enable_netexport(har_capture_path=self.init_params["capture-path"] + "/har")
        if settings.ENABLE_NOSCRIPT:
            self.pm.enable_noscript()
        if settings.ENABLE_ADBLOCK:
            self.pm.enable_adblock()
        if settings.ENABLE_GHOSTERY:
            self.pm.enable_ghostery()
        if settings.ENABLE_PRIVACY_BADGER:
            self.pm.enable_privacy_badger()
        try:
            if self.init_params["tor"]:
                logging.debug("Profile contains proxy settings for using Tor.")
                self.pm.enable_proxy(port=self.init_params["tor-port"])
        except KeyError as ex:
            logging.debug("Could not find Tor configuration parameter (%s)" % ex)
        self.crawler_state["profile"] = self.pm.get_profile()
        self.binary = FirefoxBinary(settings.FIREFOX_BINARY_PATH)
        self.driver = webdriver.Firefox(firefox_binary=self.binary, firefox_profile=self.crawler_state["profile"])
        self.driver.set_page_load_timeout(settings.DRIVER_MAX_PAGE_LOAD_TIME)
        self.crawler_state["driver"] = self.driver

    def __quit_driver(self):
        not_quit, attempts = True, 0
        while (not_quit) and (attempts <= settings.DRIVER_QUIT_RETRIES):
            try:
                self.crawler_state["driver"].quit()
                not_quit = False
            except Exception as ex:
                logging.info("Exception (%s) while quitting webdriver. Attempt: %d (of MAX: %d)." %(ex, attempts, settings.DRIVER_QUIT_RETRIES))
        return not_quit

    def __initialize_default_display(self):
        if self.init_params["display"]:
            logging.debug("Initializing default display")
            try:
                self.display = Display(visible=0, size=(1024, 768))
                self.display.start()
                self.open_pids.append(self.display.pid)
                self.crawler_state["display"] = self.display
            except Exception as ex:
                logging.info("Exception (%s) while initializing virtual display" % ex)

    def __close_display(self):
        not_closed, attempts = True, 0
        if self.crawler_state["display"] is None:
            return False
        while (not_closed) and (attempts <= settings.DISPLAY_CLOSE_RETRIES) and (self.crawler_state["display"] is not None):
            try:
                self.crawler_state["display"].stop()
                not_closed = False
            except Exception as ex:
                logging.info("Exception (%s) while closing virtual display. Attempt: %d (of MAX: %d)." %(ex, attempts, settings.DISPLAY_CLOSE_RETRIES))
                attempts += 1
        return not_closed

    def __initialize_tor_client(self):
        self.tor_params = {"port": self.init_params["tor-port"], "exit": self.init_params["tor-exit"], "tag": self.init_params["tag"]}
        self.torrc_filename = utils.create_torrc_file(self.tor_params)
        self.tor_call = Popen([settings.TOR_PATH, "-f", self.torrc_filename], preexec_fn=os.setsid)
        self.open_pids.append(self.tor_call.pid)
        self.crawler_state["tor-call"] = self.tor_call
        logging.debug("Launched Tor client. Waiting 30 seconds before resuming.")
        time.sleep(settings.TOR_INIT_TIME)

    def __kill_tor(self):
        not_killed, attempts = True, 0
        if self.crawler_state["tor-call"] is None:
            return False
        while (not_killed) and (attempts <= settings.TOR_KILL_RETRIES) and (self.crawler_state["tor-call"] is not None):
            try:
                pid = self.crawler_state["tor-call"].pid
                os.killpg(os.getpgid(pid), signal.SIGTERM)
                not_killed = False
            except Exception as ex:
                logging.info("Exception (%s) while killing Tor client. Attempt: %d (of MAX: %d)." %(ex, attempts, settings.TOR_KILL_RETRIES))
        return not_killed

    def __initialize_vpn(self, country_code):
        import vpn_manager
        selected_vpn = vpn_manager.get_vpns(country_list=[country_code])[country_code]
        logging.info("VPN: %s chosen for Country: %s" % (selected_vpn, country_code))
        status, vpn = vpn_manager.launch_vpn(vpn_params=selected_vpn)
        if status < 1:
            logging.info('Error running experiment. VPN failed to start')
            return None
        logging.info("VPN appears to have been launched successfully")
        return vpn

    def __close_vpn(self):
        import subprocess
        if self.vpn is None:
            return False
        else:
            import vpn_manager
            vpn_manager.stop_vpn(self.vpn)
            command = ['sudo', 'pkill', '-9', 'openvpn']
            subprocess.Popen(command)
            logging.info('Stopped VPN service.')
            time.sleep(1)
            return False

    def initialize_default_setup(self):
        self.__initialize_default_display()
        self.__initialize_default_webdriver()
        if self.init_params["tor"]:
            self.__initialize_tor_client()
        if self.init_params["vpn"]:
            while self.vpn is None:
                self.vpn = self.__initialize_vpn(self.init_params["vpn-loc"])
        f = open(self.init_params["capture-path"] + "/open-pids", "w")
        for pid in self.open_pids:
            f.write(str(pid) + "\n")
        f.close()

    def end_crawl(self):
        status_display, status_driver, status_tor, status_vpn = not(self.__close_display()), not(self.__quit_driver()), not(self.__kill_tor()), not(self.__close_vpn())
        self.terminated = (status_display) and (status_driver) and (status_tor) and (status_vpn)
        logging.info("Crawl Termination Status : %s [ Display | Driver | Tor ] : [ %s | %s | %s ] " % (self.terminated, status_display, status_driver, status_tor))
        self.crawler_state["driver"], self.crawler_state["display"], self.crawler_state["tor-call"], self.crawler_state["profile"] = None, None, None, None

    def get_crawler_state(self):
        return self.crawler_state

    def set_crawler_state(self, crawler_params):
        self.driver, self.profile, self.display, self.tor_call = crawler_params["driver"], crawler_params["profile"], crawler_params["display"], crawler_params["tor-call"]
        self.crawler_state = {"driver": self.driver, "display": self.display, "profile": self.profile, "tor-call": self.tor_call}

