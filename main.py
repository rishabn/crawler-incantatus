'''
Front-end for the crawler.
'''


import experiments
import logging

def main():
    ex = experiments.Experiment()
    if ex.params["action"].lower() == "front":
        logging.info ("Front page loading experiment starts.")
        ex.front_page_loading()
    elif ex.params["action"].lower() == "search":
        logging.info ("Recording interactions for using search functionality on webpages.")
        ex.record_search_rules()
    elif ex.params["action"].lower() == "login":
        logging.info ("Recording interactions for using login functionality on webpages.")
        ex.record_login_rules()
    elif ex.params["action"].lower() == "create":
        logging.info ("Recording interactions for creating accounts on webpages.")
        ex.record_account_creation_rules()
    elif ex.params["action"].lower() == "post":
        logging.info("Recording interactions from posting messages.")
        ex.record_message_posting_rules()
    elif ex.params["action"].lower() == "replay":
        logging.info("Replaying the interactions recorded in log: %s" % (ex.params["replay-log"]))
        ex.replay_interaction_log()
    elif ex.params["action"].lower() == "ads":
        logging.info("Ad-blocker experiment starting")
        ex.ad_experiment()
    elif ex.params["action"].lower() == "about-link":
        logging.info("Trying to find the \"about\" pages on websites")
        rules = [{'tag': "a", 'keywords': ["about"], 'exclusions': ["hidden"]}]
        #rules = [{'tag': "a", 'keywords': ["resources"], 'exclusions': ["hidden"]}]
        #rules = [{'tag': "a", 'keywords': ["documentation"], 'exclusions': ["hidden"]}]
        #rules = [{'tag': "a", 'keywords': ["faq"], 'exclusions': ["hidden"]}]
        ex.click_shortest_link_containing_strings_experiment(rules)
    else:
        print "Invalid option. Please use --help to see valid parameters"

if __name__ == '__main__':
    main()
