'''

Commonly used helper functions go here.

'''

import os
import json
import logging
import settings
import signal
import errno
import time
from functools import wraps


def create_folder(path):
    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except OSError as ex:
        logging.info("Exception (%s) occurred while creating folder \" %s \"" %(ex, path))


def create_torrc_file(tor_params):
    logging.debug("Creating a Torrc configuration file")
    string = "UseEntryGuards 1 \n"
    string += "SocksPort " + tor_params["port"] + "\n"
    #string += "LearnCircuitBuildTimeout 0\n"
    #string += "CircuitBuildTimeout 40\n"
    #string += "DisablePredictedCircuits 1\n"
    #string += "LeaveStreamsUnattached 1\n"
    #string += "FetchHidServDescriptors 0\n"
    #string += "UseMicroDescriptors 0\n"
    string += "MaxCircuitDirtiness 36000\n"
    string += "CircuitStreamTimeout 99999\n"
    string += "CircuitIdleTimeout 36000\n"
    string += "PathsNeededToBuildCircuits .9\n"
    if tor_params["exit"] is not None:
        if len(tor_params["exit"]) > 2:
            logging.debug("Detected that the specified exit (%s) is an IP address" % tor_params["exit"])
            string += "ExitNodes " + tor_params["exit"] + "\n"
        else:
            logging.debug("Detected that the specified exit (%s) is a country code" % tor_params["exit"])
    else:
        logging.debug("No exit mentioned. Using any exit for Tor circuits.")
    string += "StrictNodes 1 \n"
    create_folder(settings.TOR_DATA_DIRECTORY_PATH)
    create_folder(settings.TOR_DATA_DIRECTORY_PATH + tor_params["tag"])
    string += "DataDirectory " + settings.TOR_DATA_DIRECTORY_PATH + tor_params["tag"] + "\n"
    torrc = open(tor_params["tag"] + "-torrc", "w")
    torrc.write(string)
    torrc.close()
    logging.debug("Torrc file: %s" % string)
    return tor_params["tag"] + "-torrc"


def read_list_from_file(path, column=-1):
    with open(path) as f:
        lines = f.read().splitlines()
    for idx in range(0, len(lines)):
        lines[idx] = lines[idx].split(",")[-1]
    return lines


def read_credentials_from_file(path, url):
    f = open(path)
    logging.debug("Looking for login credentials for site: %s" % url)
    cred = {'user': None, 'pass': None}
    for line in f:
        line = line.split("\t")
        if line[0] not in url:
            continue
        else:
            cred = {'user': line[1], 'pass':line[2].strip("\n")}
            break
    logging.debug("Found credentials with username : %s" % cred['user'])
    return cred


class FunctionTimeoutError(Exception):
    pass


def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print '%s : %0.5f s' % (f.func_name, (time2 - time1))
        return ret
    return wrap


def timeout(seconds=settings.FUNCTION_TIMEOUT, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise FunctionTimeoutError(error_message)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result
        return wraps(func)(wrapper)
    return decorator


def dict_to_string(dictionary):
    string = None
    try:
        string = ' | '.join('({}) {}'.format(key, val) for key, val in sorted(dictionary.items()))
    except AttributeError as ex:
        logging.debug("Exception (%s) while converting dictionary to string." % ex)
    return string


def is_not_none(string):
    string = unicode(str(string).lower())
    if (string is not None) and (unicode("none") not in string):
        return True
    else:
        return False


def at_least_one_in_keywords(strings, keywords):
    in_keywords = False
    for string in strings:
        string = unicode(str(string).lower())
        if string in keywords:
            in_keywords = True
            break
    return in_keywords


def dict_to_json(dictionary):
    string = json.dumps(dictionary)
    return string


def json_to_dictionary(json_string):
    dictionary = json.loads(json_string)
    return dictionary
