'''

Add new experiments to this class.
There are currently 3: Frontpage loads, searching, and login

'''

from __future__ import division

import datetime
import argument_parser
import crawler
import settings
import logging
import data_manager
import time
import utils

class Experiment:

    def __init__(self):
        self.ap = argument_parser.ArgumentParser()
        self.params = self.ap.get_params()
        self.ci = crawler.CrawlerIncantatus(self.params)
        self.dm_params = None
        self.dm = None
        self.log = list()
        self.log_file = self.params["capture-path"] + "/experiment-status"
        self.status_file = self.params["capture-path"] + "/progress"
        f = open(self.log_file, "a")
        f.close()

    def front_page_loading(self):
        urls = self.params["sites"]
        if len(urls) == 0:
            s = open(self.status_file, "w")
            s.write("complete")
            s.close()
            return
        for idx in range(0, len(urls)):
            if idx == len(urls) - 1:
                s = open(self.status_file, "w")
                s.write("complete")
                s.close()
            else:
                s = open(self.status_file, "w")
                s.write(str(idx))
                s.close()
            self.dm_params = {'driver': self.ci.get_driver_state(), 'capture-path': self.params["capture-path"], 'filename': str(idx + self.ap.start)}
            self.dm = data_manager.DataManager(self.dm_params)
            success, attempts = False, 0
            while (not success) and (attempts <= settings.EXPERIMENT_PAGE_LOAD_RETRIES):
                self.dm.pre_load_collection()
                status = self.ci.load_page(urls[idx])
                self.dm.post_load_collection()
                success = status['load'] and status['alert'] and status['popup']
                attempts += 1
                if not success:
                    logging.debug("Restarting crawler...")
                    self.ci.restart_crawler()
                self.dm.post_load_collection()
            logging.debug(" URL: %s, Status: %s, Attempt: %d " % (urls[idx], success, attempts))
            self.log.append({'source': "frontpage-load", 'index': idx, 'url': urls[idx], 'success': str(success), 'time': str(datetime.datetime.now())})
            self.write_log_record()
            if idx % 50 == 0 and idx < len(urls):
                self.ci.restart_crawler()
        self.ci.kill_crawler()
        s = open(self.status_file, "w")
        s.write("complete")
        s.close()

    def ad_experiment(self):
        """
        An experiment which will load the front page, click on the link
        which has the longest text, scroll to the bottom, wait for a few
        seconds, and then do a post-dm record.

        Used by our ad-blocker blocking measurement paper (FOCI 2016)
        """
        urls = self.params["sites"]
        if len(urls) == 0:
            s = open(self.status_file, "w")
            s.write("complete")
            s.close()
            return
        for idx in range(0, len(urls)):
            if idx == len(urls) - 1:
                s = open(self.status_file, "w")
                s.write("complete")
                s.close()
            else:
                s = open(self.status_file, "w")
                s.write(str(idx))
                s.close()
            self.dm_params = {'driver': self.ci.get_driver_state(), 'capture-path': self.params["capture-path"], 'filename': str(idx + self.ap.start)}
            self.dm = data_manager.DataManager(self.dm_params)
            success, attempts = False, 0
            while (not success) and (attempts <= settings.EXPERIMENT_PAGE_LOAD_RETRIES):
                self.dm.pre_load_collection()
                status = self.ci.load_page(urls[idx])
                self.dm.post_load_collection()
                success = status['load'] and status['alert'] and status['popup']
                attempts += 1
                if not success:
                    logging.debug("Restarting crawler...")
                    self.ci.restart_crawler()
                else:
                    if not settings.ENABLE_NOSCRIPT:
                        self.ci.scroll_to_bottom_and_back()
                    self.ci.click_longest_link()
                time.sleep(5)
                self.dm.post_load_collection()
            logging.debug(" URL: %s, Status: %s, Attempt: %d " % (urls[idx], success, attempts))
            self.log.append({'source': "frontpage-load", 'index': idx, 'url': urls[idx], 'success': str(success), 'time': str(datetime.datetime.now())})
            self.write_log_record()
        self.ci.kill_crawler()
        s = open(self.status_file, "w")
        s.write("complete")
        s.close()

    def click_shortest_link_containing_strings_experiment(self, rules):
        """
        An experiment which will load the front page, click on the shortest
        link satisfying the rules specified and then do a post-dm record.

        Used by our "Tracking the Trackers" paper. (In submission to PETS 2017)
        """
        urls = self.params["sites"]
        for idx in range(0, len(urls)):
            s = open(self.status_file, "w")
            s.write(str(idx))
            s.close()
            self.dm_params = {'driver': self.ci.get_driver_state(), 'capture-path': self.params["capture-path"], 'filename': str(idx + self.ap.start)}
            self.dm = data_manager.DataManager(self.dm_params)
            success, attempts = False, 0
            while (not success) and (attempts <= settings.EXPERIMENT_PAGE_LOAD_RETRIES):
                #self.dm.pre_load_collection()
                status = self.ci.load_page(urls[idx])
                #self.dm.post_load_collection()
                success = status['load'] and status['alert'] and status['popup']
                attempts += 1
                if not success:
                    logging.debug("Restarting crawler...")
                    self.ci.restart_crawler()
                else:
                    logging.debug("Loaded URL. Now checking for satisfying link")
                    status = self.ci.click_shortest_link_with_rules(rules)
                    time.sleep(30)
                    if status is not None:
                        self.dm.post_load_collection()
            logging.debug(" URL: %s, Status: %s, Attempt: %d " % (urls[idx], success, attempts))
            self.log.append({'source': "frontpage-load", 'index': idx, 'url': urls[idx], 'success': str(success), 'time': str(datetime.datetime.now())})
            self.write_log_record()
        self.ci.kill_crawler()
        s = open(self.status_file, "w")
        s.write("complete")
        s.close()

    def record_search_rules(self):
        urls = self.params["sites"]
        if len(urls) == 0:
            s = open(self.status_file, "w")
            s.write("complete")
            s.close()
            return
        for idx in range(0, len(urls)):
            if idx == len(urls) - 1:
                s = open(self.status_file, "w")
                s.write("complete")
                s.close()
            else:
                s = open(self.status_file, "w")
                s.write(str(idx))
                s.close()
            self.dm_params = {'driver': self.ci.get_driver_state(), 'capture-path': self.params["capture-path"], 'filename': str(idx + self.ap.start)}
            self.dm = data_manager.DataManager(self.dm_params)
            success, attempts = False, 0
            while (not success) and (attempts <= settings.EXPERIMENT_PAGE_LOAD_RETRIES):
                self.dm.pre_load_collection()
                status = self.ci.load_page(urls[idx])
                success = status['load'] and status['alert'] and status['popup']
                attempts += 1
                if not success:
                    logging.debug("Restarting crawler...")
                    self.ci.restart_crawler()
                else:
                    interactions = self.ci.record_search_rule()
                    for i in interactions:
                        self.log.append({'source':"search", 'index':idx, 'url':urls[idx], 'interaction': i['interaction'], 'element': i['element'], 'time':str(datetime.datetime.now())})
                        self.write_log_record()
                time.sleep(10)
                self.dm.post_load_collection()
                self.ci.restart_crawler()
            #if idx % 50 == 0 and idx < len(urls):
            #    self.ci.restart_crawler()
            self.log.append({'source': "frontpage-load", 'index': idx, 'url': urls[idx], 'success': str(success), 'time': str(datetime.datetime.now())})
            self.write_log_record()
        self.ci.kill_crawler()
        s = open(self.status_file, "w")
        s.write("complete")
        s.close()

    def record_login_rules(self):
        urls = self.params["sites"]
        if len(urls) == 0:
            s = open(self.status_file, "w")
            s.write("complete")
            s.close()
            return
        for idx in range(0, len(urls)):
            if idx == len(urls) - 1:
                s = open(self.status_file, "w")
                s.write("complete")
                s.close()
            else:
                s = open(self.status_file, "w")
                s.write(str(idx))
                s.close()
            self.dm_params = {'driver': self.ci.get_driver_state(), 'capture-path': self.params["capture-path"], 'filename': str(idx + self.ap.start)}
            self.dm = data_manager.DataManager(self.dm_params)
            success, attempts = False, 0
            while (not success) and (attempts <= settings.EXPERIMENT_PAGE_LOAD_RETRIES):
                #self.dm.pre_load_collection()
                status = self.ci.load_page(urls[idx])
                success = status['load'] and status['alert'] and status['popup']
                attempts += 1
                if not success:
                    logging.debug("Restarting crawler...")
                    self.ci.restart_crawler()
                else:
                    interactions = self.ci.record_login_rules(urls[idx])
                    for i in interactions:
                        self.log.append({'source': "record-login", 'index': idx, 'url':urls[idx], 'interaction': i['interaction'], 'element': i['element'], 'time': str(datetime.datetime.now())})
                        self.write_log_record()
                self.dm.post_load_collection()
            self.ci.restart_crawler()
            self.log.append({'source': "frontpage-load", 'index': idx, 'url': urls[idx], 'success': str(success), 'time': str(datetime.datetime.now())})
            self.write_log_record()
            if idx % 50 == 0 and idx < len(urls):
                self.ci.restart_crawler()
        self.ci.kill_crawler()
        s = open(self.status_file, "w")
        s.write("complete")
        s.close()

    def record_account_creation_rules(self):
        urls = self.params["sites"]
        if len(urls) == 0:
            s = open(self.status_file, "w")
            s.write("complete")
            s.close()
            return
        for idx in range(0, len(urls)):
            if idx == len(urls) - 1:
                s = open(self.status_file, "w")
                s.write("complete")
                s.close()
            else:
                s = open(self.status_file, "w")
                s.write(str(idx))
                s.close()
            self.dm_params = {'driver': self.ci.get_driver_state(), 'capture-path': self.params["capture-path"], 'filename': str(idx + self.ap.start)}
            self.dm = data_manager.DataManager(self.dm_params)
            success, attempts = False, 0
            while (not success) and (attempts <= settings.EXPERIMENT_PAGE_LOAD_RETRIES):
                #self.dm.pre_load_collection()
                status = self.ci.load_page(urls[idx])
                success = status['load'] and status['alert'] and status['popup']
                attempts += 1
                if not success:
                    logging.debug("Restarting crawler...")
                    self.ci.restart_crawler()
                else:
                    interactions = self.ci.record_account_creation_rules(urls[idx])
                    for i in interactions:
                        self.log.append({'source': "record-creation", 'index': idx, 'url':urls[idx], 'interaction': i['interaction'], 'element': i['element'], 'time': str(datetime.datetime.now())})
                        self.write_log_record()
                self.dm.post_load_collection()
            self.ci.restart_crawler()
            self.log.append({'source': "frontpage-load", 'index': idx, 'url': urls[idx], 'success': str(success), 'time': str(datetime.datetime.now())})
            self.write_log_record()
            if idx % 50 == 0 and idx < len(urls):
                self.ci.restart_crawler()
        self.ci.kill_crawler()
        s = open(self.status_file, "w")
        s.write("complete")
        s.close()

    def record_message_posting_rules(self):
        urls = self.params["sites"]
        if len(urls) == 0:
            s = open(self.status_file, "w")
            s.write("complete")
            s.close()
            return
        for idx in range(0, len(urls)):
            if idx == len(urls) - 1:
                s = open(self.status_file, "w")
                s.write("complete")
                s.close()
            else:
                s = open(self.status_file, "w")
                s.write(str(idx))
                s.close()
            self.dm_params = {'driver': self.ci.get_driver_state(), 'capture-path': self.params["capture-path"], 'filename': str(idx + self.ap.start)}
            self.dm = data_manager.DataManager(self.dm_params)
            success, attempts = False, 0
            while (not success) and (attempts <= settings.EXPERIMENT_PAGE_LOAD_RETRIES):
                self.dm.pre_load_collection()
                status = self.ci.load_page(urls[idx])
                success = status['load'] and status['alert'] and status['popup']
                attempts += 1
                if not success:
                    logging.debug("Restarting crawler...")
                    self.ci.restart_crawler()
                else:
                    interactions = self.ci.record_message_posting_rules(urls[idx])
                    for i in interactions:
                        self.log.append({'source': "record-posting", 'index': idx, 'url':urls[idx], 'interaction': i['interaction'], 'element': i['element'], 'time': str(datetime.datetime.now())})
                        self.write_log_record()
                self.dm.post_load_collection()
            self.log.append({'source': "frontpage-load", 'index': idx, 'url': urls[idx], 'success': str(success), 'time': str(datetime.datetime.now())})
            self.write_log_record()
            if idx % 50 == 0 and idx < len(urls):
                self.ci.restart_crawler()
            self.ci.restart_crawler()
        self.ci.kill_crawler()
        s = open(self.status_file, "w")
        s.write("complete")
        s.close()

    def replay_interaction_log(self):
        urls = self.params["sites"]#self.ci.get_urls_from_replay_log()
        if len(urls) == 0:
            s = open(self.status_file, "w")
            s.write("complete")
            s.close()
            return
        count = 0
        for idx in range(0,len(urls)):
            url = urls[idx]#, d['idx']
            s = open(self.status_file, "w")
            count += 1
            s.write(str(count))
            s.close()
            self.dm_params = {'driver': self.ci.get_driver_state(), 'capture-path': self.params["capture-path"], 'filename': str(idx + self.ap.start)}
            self.dm = data_manager.DataManager(self.dm_params)
            success, attempts = False, 0
            while (not success) and (attempts <= settings.EXPERIMENT_PAGE_LOAD_RETRIES):
                #self.dm.pre_load_collection()
                status = self.ci.load_page(url)
                success = status['load'] and status['alert'] and status['popup']
                attempts += 1
                if not success:
                    logging.debug("Restarting crawler...")
                    self.ci.restart_crawler()
                else:
                    replay_status = self.ci.replay_interactions_for_url(url)
                    if not replay_status:
                        logging.info("URL: %s failed to replay correctly." % url)
                    else:
                        logging.info("URL: %s successfully replayed" % url)
                time.sleep(30)
                self.dm.post_load_collection()
            if idx % 50 == 0 and idx < len(urls):
                self.ci.restart_crawler()
            self.ci.restart_crawler()
        self.ci.kill_crawler()
        s = open(self.status_file, "w")
        s.write("complete")
        s.close()

    def write_log(self):
        f = open(self.log_file, "a")
        for record in self.log:
            f.write(utils.dict_to_json(record) + "\n")
        f.close()

    def write_log_record(self):
        f = open(self.log_file, "a")
        f.write(utils.dict_to_json(self.log[-1]) + "\n")
        f.close()
