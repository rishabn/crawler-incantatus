import time
import logging
import experiment_params
import utils
import paramiko
import scp
import dateutil.parser as dp
from html import HTML

def get_arguments():
    ex = experiment_params.ExperimentParameterGenerator()
    return ex.machines, ex.experiments, ex.storage


def check_for_completion():
    if len(to_be_scheduled) == 0:
        if len(scheduled) == 0:
            return True
    return False


def has_occupancy(machine):
    for entry in scheduled:
        if entry['machine']['ip'] == machine['ip']:
            return True
    return False


def has_vacancy(machine):
    count = 0
    for entry in scheduled:
        if machine['ip'] == entry['machine']['ip']:
            count += 1
    if count < int(machine['capacity']):
        return True
    return False


def add_entry_to_list(list_, experiment, machine):
    list_.append({'experiment': experiment, 'machine': machine, 'start_time': time.time(), 'status': None, 'last-good-load': None, 'last-download': None, 'last-read-line': None})
    return list_


def run_tbs_experiment_on_machine(tbs, machine):
    entry = tbs.pop()
    entry['machine'] = machine
    if run_entry(entry):
        scheduled.append(entry)
    return entry


def move_entry_to_complete(entry):
    scheduled.remove(entry)
    completed.append(entry)


def download_all_experiment_status(machine):
    file_list = list()
    for entry in scheduled:
        if entry['machine']['ip'] == machine['ip']:
            entry['last-download'] = time.time()
            progress_file = machine['base-path'] + entry['experiment']['status-loc'] + "/progress"
            status_file = machine['base-path'] + entry['experiment']['status-loc'] + "/experiment-status"
            log_file = machine['base-path'] + entry['experiment']['status-loc'] + "/debug.log"
            pid_file = machine['base-path'] + entry['experiment']['status-loc'] + "/open-pids"
            file_list.append(progress_file)
            file_list.append(status_file)
            file_list.append(log_file)
            file_list.append(pid_file)
        attempts, complete = 0, False
        while (attempts < 5) and (complete is False):
            try:
                attempts += 1
                ssh_client = paramiko.SSHClient()
                ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh_client.connect(machine['ip'], port=int(machine['port']), username="rishabn", look_for_keys=True)
                copy = scp.SCPClient(ssh_client.get_transport())
                for f in file_list:
                    dest_filename = temp_folder + "/" + f.split("/")[-1] + "-" + entry['experiment']['id']
                    copy.get(remote_path=f, local_path=dest_filename)
                time.sleep(1)
                copy.close()
                ssh_client.close()
                complete = True
            except Exception as ex:
                logging.debug("Exception [%s] occurred while downloading status files from remote machine" % ex)


def check_status_of_entry(entry):
    # Returns "running", "stalled", or "complete"
    filename_progress = temp_folder + "/progress-" + entry['experiment']['id']
    filename_status = temp_folder + "/experiment-status-" + entry['experiment']['id']
    line, status = "{}", "unknown"

    # If the progress file indicates the crawl is complete, return "complete". 
    # Otherwise, store the current known status as "running-<index>".
    try:
        index = 0
        f_p = open(filename_progress)
        for line in f_p:
            if line == "complete":
                entry['status'] = "complete"
                return entry['status']
            else:
                index = line
                entry['status'] = "running-" + index
        f_p.close()
    except Exception as ex:
        logging.info("Exception %s while checking status of entry" % ex)

    # Store the last line in the status file as dictionary "l".
    # If there is an exception converting to a dictionary, then store default None values.
    try:
        f_s = open(filename_status)
        l = {u'url': None, u'source': None, u'index': None, u'success': None, u'time': entry['start_time']}
        for line in f_s:
            try:
                l = utils.json_to_dictionary(line)
            except Exception as ex:
                logging.debug("Exception %s encountered" % ex)
        f_s.close()
    except Exception as ex:
        logging.info("Exception %s while checking status of entry" % ex)
        l = {u'url': None, u'source': None, u'index': None, u'success': None, u'time': entry['start_time']}

    # If the time of the last status update is valid, use it to update the "last good load"
    try:
        if l['time'] is not None:
            entry['last-good-load'] = int(time.mktime(dp.parse(l['time']).timetuple()))
    except Exception as ex:
        logging.info("Exception %s while checking parsing last-good-load time: %s. Parsing as float" % (ex, str(l['time'])))
        entry['last-good-load'] = int(l['time'])

    # If the time between the last download and the last good load is larger than expected,
    # mark the experiment as stalled. Return this status.
    try:
        diff = entry['last-download'] - entry['last-good-load']
        if diff > entry['experiment']['stall-time']:
            entry['status'] = "stalled-" + str(index) + "-" + str(diff)
            entry['last-read-line'] = l
            logging.info("Status of experiment %s is stalled. Diff: %s. Status: %s" %(entry['experiment']['id'], str(diff), entry['status']))
            return entry['status']
    except Exception as ex:
        logging.info("Exception %s while checking status of entry" % ex)

    # Return the running status.
    return entry['status']


def run_entry(entry):
    entry['start_time'] = time.time()
    cd_command = "cd " + entry['machine']['base-path'] + entry['experiment']['path'] + " ; "
    exe_command = "nohup " + entry['experiment']['params'] + " & "
    command = cd_command + exe_command
    attempts, complete = 0, False
    while (attempts < 5) and (complete is False):
        try:
            attempts += 1
            entry['start-time'] = time.time()
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh_client.connect(entry['machine']['ip'], port=int(entry['machine']['port']), username="rishabn", look_for_keys=True)
            ssh_client.exec_command(command, timeout=60)
            time.sleep(1)
            ssh_client.close()
            complete = True
        except Exception as ex:
            logging.debug("Exception [%s] occurred while shipping experiment to machine" % ex)
    return True


def kill_entry(entry):
    # Connect to remote machine and kill experiment process
    # Add the experiment to the TBS list (using current progress parameters)
    pid_file = temp_folder + "/open-pids-" + entry['experiment']['id']
    exe_command = ""
    try:
        f = open(pid_file)
        for line in f:
            exe_command += "sudo kill -9 " + line.strip() + " ; "
        f.close()
    except IOError:
        pass
    exe_command += "ps axf | grep \"" + entry['experiment']['params'] + "\" | grep -v grep | awk '{print \"sudo pkill -9 -P \" $1 \" ; sudo kill -9 \" $1 }' | sh "
    attempts, complete = 0, False
    while (attempts < 20) and (complete is False):
        try:
            attempts += 1
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh_client.connect(entry['machine']['ip'], port=int(entry['machine']['port']), username="rishabn", look_for_keys=True)
            ssh_client.exec_command(exe_command, timeout=60)
            time.sleep(1)
            ssh_client.exec_command(exe_command, timeout=60)
            logging.info("Killing experiment with command : %s" % exe_command)
            time.sleep(1)
            ssh_client.close()
            scheduled.remove(entry)
            # Look for update param in current params. Change it to old + progress
            e_params = entry['experiment']['params'].split("-")
            for param in e_params:
                if param.split()[0] == entry['experiment']['update-param']:
                    new_param = param.split()[0] + " " + str(int(param.split()[-1])+int(entry['status'].split("-")[1]) + 1) + " "
                    s = str(entry['experiment']['params'])
                    s = s.replace(param, new_param, 1)
                    entry['experiment']['params'] = s
                    entry['start_time'] = time.time()
                    entry['status'] = None
                    entry['last-good-load'] = entry['last-download']
                    entry['last-download'] = entry['last-download']
                    to_be_scheduled.append(entry)
                    logging.info("Adding new experiment %s to TBS list" % entry['experiment']['params'])
            complete = True
            time.sleep(1)
        except Exception as ex:
            logging.info("Exception [%s] occurred while killing stalled experiment" % ex)


def generate_html_status_file():
    html_log = "./experiment-status.html"
    page = HTML('html')
    page.script("",src="sorttable.js")
    page.p("Scheduled Experiments")
    table = page.table(border='1', klass="sortable")
    row = table.tr
    index = 0
    row.td("Experiment ID")
    row.td("Elapsed time (hrs)")
    row.td("Parameters")
    row.td("Machine IP")
    row.td("Status")
    row.td("Status Link")
    for entry in scheduled:
        row = table.tr
        index += 1
        row.td(str(entry['experiment']['id']))
        try:
            elapsed_time = time.time() - entry['start_time']
            row.td(str(elapsed_time/3600))
        except Exception as ex:
            logging.info("Exception: %s" % ex)
        row.td(str(entry['experiment']['params']))
        row.td(str(entry['machine']['ip']))
        row.td(str(entry['status']))
        log_file = temp_folder + "/debug.log-" + entry['experiment']['id']
        row.td.a(str(log_file), href=log_file)
    page.p("To be scheduled experiments")
    table = page.table(border='1', klass="sortable")
    row = table.tr
    index = 0
    row.td("Experiment ID")
    row.td("Parameters")
    for entry in to_be_scheduled:
        row = table.tr
        index += 1
        row.td(str(entry['experiment']['id']))
        row.td(str(entry['experiment']['params']))
    page.p("Completed experiments")
    table = page.table(border='1', klass="sortable")
    row = table.tr
    index = 0
    row.td("Experiment ID")
    row.td("Parameters")
    for entry in completed:
        row = table.tr
        index += 1
        row.td(str(entry['experiment']['id']))
        row.td(str(entry['experiment']['params']))
    o = open(html_log, "w")
    o.write(str(page))
    o.flush()
    o.close()
    return None


def main():
    all_complete = False
    current_time, previous_time = time.time(), time.time()
    iteration = 0
    while all_complete is False:
        all_complete = check_for_completion()
        if all_complete:
            logging.info("All experiments are complete!")
            break
        for machine in machines:
            previous_time = current_time
            current_time = time.time()
            elapsed_time = current_time - previous_time
            iteration += 1
            logging.info("Iteration: %d | Previous iteration took : %d seconds" % (iteration, elapsed_time))
            while (has_vacancy(machine) is True) and (len(to_be_scheduled) > 0):
                entry = run_tbs_experiment_on_machine(to_be_scheduled, machine)
                logging.info("Scheduled an experiment [%s|%s] on machine %s" % (entry['experiment']['id'], entry['experiment']['params'], entry['machine']['ip']))
            if has_occupancy(machine) is True:
                download_all_experiment_status(machine)
                logging.info("Downloading the status of all experiments running on machine %s" % machine['ip'])
            for entry in scheduled:
                if entry['machine']['ip'] == machine['ip']:
                    check_status_of_entry(entry)
                    logging.info("Status of experiment [%s|%s] on machine %s is %s" % (entry['experiment']['id'], entry['experiment']['params'], entry['machine']['ip'], entry['status']))
                    try:
                        if entry['status'] == "complete":
                            move_entry_to_complete(entry)
                        elif "stalled" in entry['status']:
                            kill_entry(entry)
                        else:
                            continue
                    except TypeError:
                        continue
            generate_html_status_file()
        for entry in scheduled:
            logging.info("Experiment: %s | %s | Scheduled" % (entry['experiment']['id'], entry['experiment']['params']))
        for entry in to_be_scheduled:
            logging.info("Experiment: %s | %s | To be scheduled" % (entry['experiment']['id'], entry['experiment']['params']))
        for entry in completed:
            logging.info("Experiment: %s | %s | Complete" % (entry['experiment']['id'], entry['experiment']['params']))


if __name__ == '__main__':
    machines, experiments, temp_folder = get_arguments()
    to_be_scheduled, scheduled, completed = list(), list(), list()
    for e in experiments:
        to_be_scheduled = add_entry_to_list(to_be_scheduled, e, None)
    for machine in machines:
        while (has_vacancy(machine) is True) and (len(to_be_scheduled) > 0):
                entry = run_tbs_experiment_on_machine(to_be_scheduled, machine)
                logging.info("Scheduled an experiment [%s|%s] on machine %s" % (entry['experiment']['id'], entry['experiment']['params'], entry['machine']['ip']))
    main()
