'''

Commonly used helper functions go here.

'''

import os
import json
import logging


def create_folder(path):
    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except OSError as ex:
        logging.info("Exception (%s) occurred while creating folder \" %s \"" %(ex, path))


def delete_folder_contents(path):
    try:
        logging.info("Deleting contents of folder: %s" % path)
        for f in os.listdir(path):
            fp = os.path.join(path, f)
            if os.path.isfile(fp):
                os.remove(fp)
    except OSError as ex:
        logging.info("Exception (%s) occurred while deleting contents of folder \" %s \"" %(ex, path))


def read_list_from_file(path, column=-1):
    with open(path) as f:
        lines = f.read().splitlines()
    for idx in range(0, len(lines)):
        lines[idx] = lines[idx].split(",")[-1]
    return lines


def dict_to_string(dictionary):
    string = None
    try:
        string = ' | '.join('({}) {}'.format(key, val) for key, val in sorted(dictionary.items()))
    except AttributeError as ex:
        logging.debug("Exception (%s) while converting dictionary to string." % ex)
    return string


def is_not_none(string):
    string = unicode(str(string).lower())
    if (string is not None) and (unicode("none") not in string):
        return True
    else:
        return False


def dict_to_json(dictionary):
    string = json.dumps(dictionary)
    return string


def json_to_dictionary(json_string):
    dictionary = json.loads(json_string)
    return dictionary