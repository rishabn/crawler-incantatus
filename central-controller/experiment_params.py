import utils
import argparse
import logging
import sys


class ExperimentParameterGenerator():

    def __init__(self):
        args = self.__parse_args()
        root = logging.getLogger()
        if root.handlers:
            for handler in root.handlers:
                root.removeHandler(handler)
        if args.debug:
            logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)-8s [CENTRAL] %(filename)s:%(lineno)-4d: %(message)s', datefmt='%m-%d %H:%M')
            logging.info("Debug level logging info enabled")
        else:
            logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-8s [CENTRAL] %(filename)s:%(lineno)-4d: %(message)s', datefmt='%m-%d %H:%M')
        self.machines = list()
        try:
            m = open(args.machines, "r")
            for line in m:
                if line.startswith("#"):
                    continue
                machine = dict()
                parameters = line.split(",")
                machine['ip'], machine['port'], machine['capacity'], machine['base-path'] = parameters[0].strip(), parameters[1].strip(), parameters[2].strip(), parameters[3].strip()
                self.machines.append(machine)
                logging.debug("Added machine: %s" % machine['ip'])
            m.close()
        except Exception as ex:
            logging.info("Exception (%s) occurred while opening machine configuration file." % ex)
            sys.exit(0)
        self.experiments = list()
        try:
            e = open(args.experiments, "r")
            for line in e:
                if line.startswith("#"):
                    continue
                experiment = dict()
                parameters = line.split(",")
                print parameters
                experiment['id'], experiment['path'], experiment['params'], experiment['status-loc'], experiment['stall-time'], experiment['update-param'] = parameters[0].strip(), parameters[1].strip(), parameters[2].strip(), parameters[3].strip(), int(parameters[4].strip()), parameters[5].strip()
                self.experiments.append(experiment)
                logging.debug("Added experiment: %s" % experiment['id'])
            e.close()
        except Exception as ex:
            logging.info("Exception (%s) occurred while opening experiment configuration file." % ex)
            sys.exit(0)
        print self.machines
        print self.experiments
        self.storage = args.storage
        utils.delete_folder_contents(self.storage)
        utils.create_folder(self.storage)

    @staticmethod
    def __parse_args(print_help=False):
        parser = argparse.ArgumentParser()
        parser.add_argument('--debug', '-d', help='Enable debug logging', action='store_true')
        parser.add_argument('--machines', '-m', help='Machine IPs, ssh ports, capacities, and base program paths', dest='machines', required=True)
        parser.add_argument('--experiments', '-e', help='Experiment ID, path (start from base), and command line execution parameters', dest='experiments', required=True)
        parser.add_argument('--data-folder', '-df', help='Location of the folder that the controller will use for file transfers', dest='storage', required=True)
        parsed_args = parser.parse_args()
        if print_help:
            parser.print_help()
            sys.exit(0)
        return parsed_args
