'''

Functions dealing with login/authentication interactions go in this class.
Add new templates for authentication interactions here.

'''

import utils
import logging
import settings
import dom_interactions
from selenium.webdriver.common.keys import Keys

class AuthenticationInteractions:

    def __init__(self, driver):
        self.driver = driver
        self.di = dom_interactions.DomInteractions(driver)
        self.compatible = self.__check_compatibility()
        self.interactions = list()

    def __check_compatibility(self):
        logging.debug("Checking compatibility of URL for auto-login")
        rules = list()
        keywords = ["signin", "login"]
        tags = ["a", "input", "button"]
        exclusions = ["hidden", "next"]
        for t in tags:
            rules.append({'tag': t, 'keywords': keywords, 'exclusions': exclusions})
        status = self.di.does_page_satisfy_rules(rules)
        if True in status:
            return True
        else:
            return False

    def __base_interaction(self):
        logging.debug("Clicking element that is most similar to a sign in link")
        rules = list()
        keywords = ["signin", "login"]
        tags = ["a", "input", "button"]
        exclusions = ["hidden", "next"]
        for t in tags:
            rules.append({'tag': t, 'keywords': keywords, 'exclusions': exclusions})
        clicked_element_attributes = self.di.click_closest_satisfying_element(rules)
        if clicked_element_attributes is not None:
            self.interactions.append({'interaction':"click", 'element': clicked_element_attributes})

    def __get_current_page_status(self):
        logging.debug("Getting the current page status")
        password_keywords, login_keywords, next_keywords, facebook_keywords = ["password", "pass"], ["signin", "login"], ["next", "continue"], ["loginwithfacebook", "continuewithfacebook"]
        password_tags, login_tags, next_tags, facebook_tags = ["input"], ["input", "a", "button"], ["input", "a", "button"], ["input", "a", "button"]
        password_exclude, login_exclude, next_exclude, facebook_exclude = ["hidden"], ["hidden", "next"], ["hidden"], ["hidden"]
        password_rules, login_rules, next_rules, facebook_rules = list(), list(), list(), list()
        for pt in password_tags:
            password_rules.append({'tag': pt, 'keywords': password_keywords, 'exclusions': password_exclude})
        password_status = self.di.does_page_satisfy_rules(password_rules)
        if True in password_status:
            password_status = True
        else:
            password_status = False
        for lt in login_tags:
            login_rules.append({'tag': lt, 'keywords': login_keywords, 'exclusions': login_exclude})
        login_status = self.di.does_page_satisfy_rules(login_rules)
        if True in login_status:
            login_status = True
        else:
            login_status = False
        for nt in next_tags:
            next_rules.append({'tag': nt, 'keywords': next_keywords, 'exclusions': next_exclude})
        next_status = self.di.does_page_satisfy_rules(next_rules)
        if True in next_status:
            next_status = True
        else:
            next_status = False
        for ft in facebook_tags:
            facebook_rules.append({'tag': ft, 'keywords': facebook_keywords, 'exclusions': facebook_exclude})
        facebook_status = self.di.does_page_satisfy_rules(facebook_rules)
        if True in facebook_status:
            facebook_status = True
        else:
            facebook_status = False
        logging.info("Current page status: Password %s, Login %s, Next: %s, Facebook Login: %s" % (password_status, login_status, next_status, facebook_status))
        return {'password': password_status, 'login': login_status, 'next': next_status, 'facebook': facebook_status}

    def single_page_signin(self, credentials):
        return self.__standard_single_page_signin(credentials)

    def __standard_single_page_signin(self, credentials):
        logging.info("Detected a standard single signin page")
        username_keywords, password_keywords = ["email", "user", "name", "id", "login_field"], ["password", "pass"]
        username_tags, password_tags = ["input"], ["input"]
        username_exclude, password_exclude = ["hidden"], ["hidden"]
        username_rules, password_rules = list(), list()
        for ut in username_tags:
            username_rules.append({'tag': ut, 'keywords': username_keywords, 'exclusions': username_exclude})
        for pt in password_tags:
            password_rules.append({'tag': pt, 'keywords': password_keywords, 'exclusions': password_exclude})
        user_field = self.di.does_page_satisfy_rules(username_rules)
        if True in user_field:
            user_field = True
        else:
            user_field = False
        user_status = self.di.send_text_to_all_rule_satisfying_elements(username_rules, credentials['user'])
        if user_status is None:
            logging.debug("Failed while entering username")
            return False
        else:
            for us in user_status:
                if us is None:
                    continue
                self.interactions.append({'interaction':"type|"+credentials['user'], 'element': us})
        pass_status = self.di.send_text_to_all_rule_satisfying_elements(password_rules, credentials['pass'])
        if pass_status is None:
            logging.debug("Failed while entering password")
            return False
        else:
            for ps in pass_status:
                if ps is None:
                    continue
                self.interactions.append({'interaction':"type|"+credentials['pass'], 'element': ps})
        return_status = self.di.send_text_to_all_rule_satisfying_elements(password_rules, Keys.RETURN, max_count=1)
        if return_status is None:
            logging.debug("Failed while submitting form (via RETURN)")
            return False
        else:
            for rs in return_status:
                self.interactions.append({'interaction':"return", 'element': rs})
        logging.info("Succeeded at logging in!")
        return True

    def __standard_two_page_signin(self, credentials):
        logging.info("Detected a standard two page signin process")
        username_keywords, password_keywords, next_keywords = ["email", "user", "name", "id", "login_field"], ["password", "pass"], ["next", "continue"]
        username_tags, password_tags, next_tags = ["input"], ["input"], ["input", "a", "button"]
        username_exclude, password_exclude, next_exclude = ["hidden"], ["hidden"], ["hidden"]
        username_rules, password_rules, next_rules = list(), list(), list()
        for ut in username_tags:
            username_rules.append({'tag': ut, 'keywords': username_keywords, 'exclusions': username_exclude})
        for pt in password_tags:
            password_rules.append({'tag': pt, 'keywords': password_keywords, 'exclusions': password_exclude})
        for nt in next_tags:
            next_rules.append({'tag': nt, 'keywords': next_keywords, 'exclusions': next_exclude})
        user_status = self.di.send_text_to_all_rule_satisfying_elements(username_rules, credentials['user'])
        if user_status is None:
            logging.debug("Failed while entering username")
            return False
        else:
            for us in user_status:
                if us is None:
                    continue
                self.interactions.append({'interaction':"type|"+credentials['user'], 'element': us})
        next_status = self.di.click_closest_satisfying_element(next_rules)
        if next_status is None:
            logging.debug("Failed while clicking NEXT button/link")
            return False
        else:
            self.interactions.append({'interaction': "click", 'element': next_status})
        pass_status = self.di.send_text_to_all_rule_satisfying_elements(password_rules, credentials['pass'])
        if pass_status is None:
            logging.debug("Failed while entering password")
            return False
        else:
            for ps in pass_status:
                if ps is None:
                    continue
                self.interactions.append({'interaction':"type|"+credentials['pass'], 'element': ps})
        return_status = self.di.send_text_to_all_rule_satisfying_elements(password_rules, Keys.RETURN, max_count=1)
        if return_status is None:
            logging.debug("Fauked while submitting form (via RETURN)")
            return False
        else:
            for rs in return_status:
                self.interactions.append({'interaction':"return", 'element': rs})
        logging.info("Succeeded at logging in!")
        return True

    def __facebook_login(self, credentials):
        # Click button/link containing "log in with facebook"
        logging.debug("Clicking element that is most similar to a facebook login link")
        rules = list()
        logged_in = False
        keywords = ["signinwithfacebook", "loginwithfacebook"]
        tags = ["a", "input", "button"]
        exclusions = ["hidden"]
        for t in tags:
            rules.append({'tag': t, 'keywords': keywords, 'exclusions': exclusions})
        main_window_handle = None
        while not main_window_handle:
            main_window_handle = self.driver.current_window_handle
        clicked_element_attributes = self.di.click_closest_satisfying_element(rules)
        if clicked_element_attributes is not None:
            self.interactions.append({'interaction':"click", 'element': clicked_element_attributes})
        else:
            logged_in = False
            return logged_in
        signin_window_handle = None
        if len(self.driver.window_handles) > 1:
            while not signin_window_handle:
                for handle in self.driver.window_handles:
                    if handle != main_window_handle:
                        signin_window_handle = handle
                        break
            if signin_window_handle:
                self.driver.switch_to.window(signin_window_handle)
                self.interactions.append({'interaction':"switch-to-popup", 'element': None})
                logged_in = self.__standard_single_page_signin(credentials)
        else:
            # if no pop-up, do single-page sign-in
            self.driver.switch_to.window(main_window_handle)
            logged_in = self.__standard_single_page_signin(credentials)
        self.driver.switch_to.window(main_window_handle)
        return logged_in

    def record_login_interaction(self, credentials):
        logging.debug("Recording interactions required to perform an auto-login")
        logged_in = False
        if not self.compatible:
            return logged_in
        self.__base_interaction()
        current_interactions = len(self.interactions)
        while (not logged_in) and (current_interactions <= settings.DRIVER_LOGIN_MAX_INTERACTIONS):
            current_interactions = len(self.interactions)
            page_status = self.__get_current_page_status()
            if (page_status['facebook']) and (credentials['user'].split("|")[0] == "facebook"):
                credentials['user'] = credentials['user'].split("|")[1]
                logged_in = self.__facebook_login(credentials)
                logging.debug("Login status: %s" % logged_in)
                return logged_in
            if page_status['password']:
                # Fill in all fields and hit ENTER
                logged_in = self.__standard_single_page_signin(credentials)
                logging.debug("Login status: %s " % logged_in)
                return logged_in
            if page_status['next'] and not page_status['password']:
                # Fill in all username fields and click NEXT
                logged_in = self.__standard_two_page_signin(credentials)
                logging.debug("Login status: %s " % logged_in)
                return logged_in
            if page_status['login'] and not page_status['next']:
                # Press the LOGIN button and see if one of the above conditions becomes True
                self.__base_interaction()
        logging.debug("Login status: %s " % logged_in)
        return logged_in

