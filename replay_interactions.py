'''

__author__ = "Rishab Nithyanand"
__email__ = "rnithyanand@cs.stonybrook.edu"

This class contains all replay functions.

Right now it supports: replaying clicks, sending username,
password, and search query text.

'''

import time
import dom_interactions
import logging
import utils
from selenium.common.exceptions import NoSuchWindowException

class ReplayInteractions:

    def __init__(self, driver, log):
        logging.debug("Setting up parameters for replay mode")
        self.driver = driver
        self.di = dom_interactions.DomInteractions(driver)
        self.log = log
        logging.debug("Replaying log: %s" % utils.dict_to_json(self.log))
        self.status = False
        self.__replay_log()

    def __replay_log(self):
        for l in self.log:
            time.sleep(.5)
            try:
                if l['element'] is not None:
                    element_id = l['element']['id']
                    logging.debug("Element to be interacted with has id %s" % element_id)
                else:
                    element_id = None
                logging.debug("Replaying interaction record: %s" % utils.dict_to_json(l))
                if l['interaction'] == "click":
                    interaction_status = self.di.click_element_with_id(element_id)
                    if interaction_status:
                        logging.debug("Successfully clicked element with ID: %s" % element_id)
                        self.status = True
                        continue
                    else:
                        logging.debug("Failed to replay interactions due to failed element click.")
                        self.status = False
                if "type" in l['interaction']:
                    text = l['interaction'].split("|")[-1]
                    interaction_status = self.di.send_text_to_element_with_id(text, element_id)
                    if interaction_status:
                        logging.debug("Successfully sent %s (text) to element with ID: %s" % (text, element_id))
                        self.status = True
                        continue
                    else:
                        logging.debug("Failed to replay interactions due to failed text send.")
                        self.status = False
                if l['interaction'] == "return":
                    from selenium.webdriver.common.keys import Keys
                    interaction_status = self.di.send_text_to_element_with_id(Keys.RETURN, element_id)
                    if interaction_status:
                        logging.debug("Successfully sent RETURN to element with ID: %s" % (element_id))
                        self.status = True
                        continue
                    else:
                        logging.debug("Failed to replay interactions due to failed RETURN send.")
                        self.status = False
                if l['interaction'] == "switch-to-popup":
                    time.sleep(.5)
                    try:
                        self.driver.switch_to_window(self.driver.window_handles[-1])
                    except NoSuchWindowException as ex:
                        logging.debug("Exception (%s) encoutnered while switching windows (an expected popup likely did not appear" % ex)
            except:
                continue
